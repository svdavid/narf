function plot_linear_kernel_shootoff(batch, cellids, modelnames)
% Puts all metrics' pareto plots on the same figure
dbopen;
modelnames = {{'fb18ch100_lognn_wc01_fir15_siglog100_fit05h_fit05c', ...
               'fb18ch100_lognn_wc02_fir15_siglog100_fit05h_fit05c', ...
               'fb18ch100_lognn_wc03_fir15_siglog100_fit05h_fit05c', ...
               'fb18ch100_lognn_wc04_fir15_siglog100_fit05h_fit05c'}, ...
              {'fb18ch100_lognn_wcg01_fir15_siglog100_fit05h_fit05c', ...
               'fb18ch100_lognn_wcg02_fir15_siglog100_fit05h_fit05c', ...
               'fb18ch100_lognn_wcg03_fir15_siglog100_fit05h_fit05c', ...
               'fb18ch100_lognn_wcg04_fir15_siglog100_fit05h_fit05c'}, ...
              {'fb18ch100_lognn_wcg01_fir15p_siglog100_fit05h_fit19g', ...
               'fb18ch100_lognn_wcg02_fir15p_siglog100_fit05h_fit19g', ...
               'fb18ch100_lognn_wcg03_fir15p_siglog100_fit05h_fit19g', ...
               'fb18ch100_lognn_wcg04_fir15p_siglog100_fit05h_fit19g'}, ...
              {'fb18ch100_lognn_wcg01_ap3z1_dexp_fit09c', ...
               'fb18ch100_lognn_wcg02_ap3z1_dexp_fit09c', ...
               'fb18ch100_lognn_wcg03_ap3z1_dexp_fit09c', ...
               'fb18ch100_lognn_wcg04_ap3z1_dexp_fit09c', ...
               'fb18ch100_lognn_wcg05_ap3z1_dexp_fit09c'}, ...      
              {'fb18ch100_lognn_wcg01_ap2z1_dexp_fit09c', ...
               'fb18ch100_lognn_wcg02_ap2z1_dexp_fit09c', ...
               'fb18ch100_lognn_wcg03_ap2z1_dexp_fit09c', ...
               'fb18ch100_lognn_wcg04_ap2z1_dexp_fit09c', ...
               'fb18ch100_lognn_wcg05_ap2z1_dexp_fit09c'}, ...
              {'fb18ch100_lognn_wcg01_ap2z0_dexp_fit09c', ...
               'fb18ch100_lognn_wcg02_ap2z0_dexp_fit09c', ...
               'fb18ch100_lognn_wcg03_ap2z0_dexp_fit09c', ...
               'fb18ch100_lognn_wcg04_ap2z0_dexp_fit09c', ...
               'fb18ch100_lognn_wcg05_ap2z0_dexp_fit09c', ...
               'fb18ch100_lognn_wcg06_ap2z0_dexp_fit09c'}, ...
              {'fb18ch100_lognn_wcg01_ap1z0_dexp_fit09c', ...
               'fb18ch100_lognn_wcg02_ap1z0_dexp_fit09c', ...
               'fb18ch100_lognn_wcg03_ap1z0_dexp_fit09c', ...
               'fb18ch100_lognn_wcg04_ap1z0_dexp_fit09c', ...
               'fb18ch100_lognn_wcg05_ap1z0_dexp_fit09c', ...
               'fb18ch100_lognn_wcg06_ap1z0_dexp_fit09c'}};           

metrics = {'r_test'};
n_tall = 1;
n_wide = 1;
subplot = @(m,n,p) subtightplot (m, n, p, [0.01 0.1], [0.1 0.03], [0.1 0.03]);

figure();

for pp = 1:length(metrics)
    subplot(n_tall,n_wide,pp);
    hold on;
    metric = metrics{pp};
       
    for ii = 1:length(modelnames)
        c = pickcolor(ii);       
       
        xs = [];
        means = [];
        meds = [];
        d1=[];
        d2=[];
        d3=[];
        d4=[];
        d5=[];
        d6=[];
        d7=[];
        d8=[];
        d9=[];
        for jj = 1:length(modelnames{ii})            
            p = pickpoint(jj);
            model = modelnames{ii}{jj};
    
            sql = ['SELECT * FROM NarfResults WHERE batch=' num2str(batch)];
            sql = [sql ' AND modelname="' model '"'];
            results = mysql(sql);                        
            
            if length(results) ~= 176
                continue;
            end
            
            x = [results.n_parms]; 
            y = [results.(metric)];
            %plot(x + 0.2*rand(1, length(x)) - 0.1, y, 'Color', c, 'Marker', '.', 'Linestyle', 'none')
            xs(jj) = nanmean(x);
            %means(jj) = nanmean(y);            
            
            ys = sort(y);
            n_y = length(ys);
%             d1(jj) = nanmean(ys(ceil(n_y*0.1):floor(n_y*0.2)));
%             d2(jj) = nanmean(ys(ceil(n_y*0.2):floor(n_y*0.3)));
%             d3(jj) = nanmean(ys(ceil(n_y*0.3):floor(n_y*0.4)));
%             d4(jj) = nanmean(ys(ceil(n_y*0.4):floor(n_y*0.5)));
%             d5(jj) = nanmean(ys(ceil(n_y*0.5):floor(n_y*0.6)));
%             d6(jj) = nanmean(ys(ceil(n_y*0.6):floor(n_y*0.7)));
%             d7(jj) = nanmean(ys(ceil(n_y*0.7):floor(n_y*0.8)));
%             d8(jj) = nanmean(ys(ceil(n_y*0.8):floor(n_y*0.9)));
%             d9(jj) = nanmean(ys(ceil(n_y*0.9):floor(n_y*1.0)));
            
            inner = ys(ceil(n_y*0.1):floor(n_y*0.9)); % Take inner 80%
            means(jj) = nanmean(inner);
            %means(jj) = nanmean(y);
            %meds(jj)  = nanmedian(y); 
            
        end
        %
        %         plot(xs, d1, 'Color', c, 'Marker', '.', 'Linestyle', '-');
        %         plot(xs, d2, 'Color', c, 'Marker', '.', 'Linestyle', '-');
        %         plot(xs, d3, 'Color', c, 'Marker', '.', 'Linestyle', '-');
        %         plot(xs, d4, 'Color', c, 'Marker', '.', 'Linestyle', '-');
        %         plot(xs, d5, 'Color', c, 'Marker', '.', 'Linestyle', '-');
        %         plot(xs, d6, 'Color', c, 'Marker', '.', 'Linestyle', '-');
        %         plot(xs, d7, 'Color', c, 'Marker', '.', 'Linestyle', '-');
        %         plot(xs, d8, 'Color', c, 'Marker', '.', 'Linestyle', '-');
        %         plot(xs, d9, 'Color', c, 'Marker', '.', 'Linestyle', '-');
        
        plot(xs, means,  'Color', c, 'Marker', '.', 'Linestyle', '-');                
        %plot(xs, meds,  'Color', c, 'Marker', 'x', 'Linestyle', '--');                
        
        %plot(1:length(ys), ys, 'Color', c, 'Marker', '.', 'Linestyle', '-');
        
    end
    hold off;
    
    xlabel('Parameters');
    ylabel(metric, 'Interpreter', 'none');   
        
    textLoc(sprintf('WC+FIR'), 'NorthEast', 'Color', pickcolor(1));
    textLoc(sprintf('\nWCG+FIR'), 'NorthEast', 'Color', pickcolor(2));
    textLoc(sprintf('\n\nWCG+FIRP'), 'NorthEast', 'Color', pickcolor(3));
    textLoc(sprintf('\n\n\nWCG+AP3Z1'), 'NorthEast', 'Color', pickcolor(4));    
    textLoc(sprintf('\n\n\n\nWCG+AP2Z1'), 'NorthEast', 'Color', pickcolor(5));  
    textLoc(sprintf('\n\n\n\n\nWCG+AP2Z0'), 'NorthEast', 'Color', pickcolor(6));
    textLoc(sprintf('\n\n\n\n\n\nWCG+AP1Z0'), 'NorthEast', 'Color', pickcolor(7));
    
end
