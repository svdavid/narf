function plot_data_curves_by_region(batch, cellids, modelnames)
% Looks for a connection between data quality and model performance

if length(modelnames) > 1
    error('Only one modelname may be selected for this script');
end

metric = 'r_ceiling';

batches = {267, 266, 264};

% Load the modelnames and add them to the data
testdata = nan(length(batches), length(modelnames), 500);
fitdata  = nan(length(batches), length(modelnames), 500);

n_cells = 0;
for bb = 1:length(batches)
    batch = batches{bb};
    
    sql = ['SELECT distinct cellid from sRunData WHERE batch=', num2str(batch)];
	ret = mysql(sql);
    cellids = {ret.cellid};
    
    for jj = 1:length(cellids)
        % TEMPORARY HACK: Remove troublesome cells causing request celldb
        % to crash
        if any(strcmp(cellids{jj}, {'daf076e-b1', 'daf077b-a1', 'daf077b-d1', 'dai035a-c1'}))
            continue;
        end
        s = request_celldb_batch(batch, cellids{jj});
        v_fit = dbgetscellfile('cellid', cellids{jj}, 'rawid', s{1}.training_rawid);
        v_test = dbgetscellfile('cellid', cellids{jj}, 'rawid', s{1}.test_rawid);
        
        [snr_val,snr_est, z_val, z_est, sec_val, sec_est]=db_get_snr(cellids{jj},batch);  
        % Crop out the bad cells with very low SNR
        if isempty(snr_val) || isempty(snr_est) || snr_est < 0.15 || snr_val < 0.15
            continue;
        end
        n_cells = n_cells + 1;
        
        for ii = 1:length(modelnames)                   
            sql = ['SELECT * FROM NarfResults WHERE batch=' num2str(batch)];
            sql = [sql ' AND modelname="' modelnames{ii} '" AND cellid="' cellids{jj} '"'];
            r = mysql(sql);
            
            if length(v_test) ~= 1
                v_test = v_test(1);
                fprintf('WARNING!!!!');
            end
            if length(v_fit) ~= 1
                fprintf('WARNING!!!!');
                v_fit = v_fit(1);
            end
            
            if isempty(v_test.isolation)
                v_test.isolation = NaN;
            end
            
            if isempty(v_fit.isolation)
                v_fit.isolation = NaN;
            end      
            
            % TEMPORARY HACK: Remove an outlier
           %if v_fit.reps == 1 && v_test.reps == 20
           %     continue;
           % end               
           %
           % TEMPORARY HACK: Remove outliers
           %if (v_fit.reps * snr_est < 0.1)
           %    continue;
           %end
                     
            if ~isempty(snr_val) && ~isempty(snr_est)
                fitdata(bb,ii,jj,7) = snr_est;
                testdata(bb,ii,jj,7) = snr_val;           
                fitdata(bb,ii,jj,8) = z_est;
                testdata(bb,ii,jj,8) = z_val;         
                fitdata(bb,ii,jj,9) = sec_est;
                testdata(bb,ii,jj,9) = sec_val;  
            end                        
            
            fitdata(bb,ii,jj,1) = v_fit.reps;
            fitdata(bb,ii,jj,2) = v_fit.spikes;
            fitdata(bb,ii,jj,3) = v_fit.isolation;
            fitdata(bb,ii,jj,4) = v_fit.spikes / v_fit.reps;
            
            if ~isempty(r)
                fitdata(bb,ii,jj,5) = r(1).r_test / r(1).r_ceiling;
                fitdata(bb,ii,jj,6) = r(1).r_fit.^2;
            end
            
            testdata(bb,ii,jj,1) = v_test.reps;
            testdata(bb,ii,jj,2) = v_test.spikes;
            testdata(bb,ii,jj,3) = v_test.isolation;
            testdata(bb,ii,jj,4) = v_test.spikes / v_test.reps;
            
            if ~isempty(r)
                testdata(bb,ii,jj,5) = r(1).r_test / r(1).r_ceiling;
                %testdata(bb,ii,jj,6) = r(1).r_test .^2;
                testdata(bb,ii,jj,6) = r(1).r_ceiling .^2;
            end
            
        end
    end
end

%testdata = reshape(testdata, [], 9);
%fitdata = reshape(fitdata, [], 9);

b1 = excise(reshape(squeeze(testdata(1,:,:,:)), [], 9));
b2 = excise(reshape(squeeze(testdata(2,:,:,:)), [], 9));
b3 = excise(reshape(squeeze(testdata(3,:,:,:)), [], 9));

    function plotit(x, y, z, color)
        hold on
        plot(x, y, 'Color', color, 'LineStyle', '.');
        d = sortrows([x y]);
        gf = gausswin(2*ceil(length(x)/5));
        gf = gf / sum(gf);                              
        plot(iconv(d(:, 1), gf), iconv(d(:, 2), gf),  'Color', color, 'LineStyle', '-');        
        %p = polyfit(x,y,1);title('FitSNR vs TestSNR')
        %plot(x, p(1)*x + p(2), color);                              
        hold off;
        
        figure; 
        ix = 1./(x.^2 .*z);
        iy = 1./y;
        p = polyfit(ix,iy,1); 
        hold on;
        plot(ix, iy, 'k.'); 
        plot (ix, p(1).*ix + p(2), 'r-');
        title('FitSNR vs TestSNR')
        xlabel('1/(T*SNR^2)');
        ylabel('1/(R_{ceil}^2)');
        p
        hold off;
    end

figure;
% %%%%%%%%%%%%%%%

hold on;
plotit(b1(:, 7), b1(:, 6), b1(:, 1), 'r');
%plotit(b1(:, 7) .* b1(:, 1), b1(:, 6), b1(:, 1) 'r');
%plotit(b2(:, 7) .* b2(:, 1), b2(:, 6), 'g');
%plotit(b3(:, 7) .* b3(:, 1), b3(:, 6), 'b');
title('FitSNR*FitReps vs R_{ceiling}^2');
xlabel('Fit SNR * Fit Reps');
ylabel('R_{ceiling}^2');
legend('267 (A1)', '', '266 (PEG)', '', '264 (PPF)', '');     
hold off

end
