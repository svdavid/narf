function plot_data_in_limit(batch, cellids, modelnames)

metric = 'r_ceiling';

% Load the modelnames and add them to the data
testdata = nan(length(modelnames), 500);
fitdata  = nan(length(modelnames), 500);
n_cells = 0;

sql = ['SELECT distinct cellid from sRunData WHERE batch=', num2str(batch)];
ret = mysql(sql);
cellids = {ret.cellid};

for jj = 1:length(cellids)
    
    s = request_celldb_batch(batch, cellids{jj});
    v_fit = dbgetscellfile('cellid', cellids{jj}, 'rawid', s{1}.training_rawid);
    v_test = dbgetscellfile('cellid', cellids{jj}, 'rawid', s{1}.test_rawid);
    
    [snr_val,snr_est, z_val, z_est, sec_val, sec_est]=db_get_snr(cellids{jj},batch);
    
    % Crop out the bad cells with very low SNR
    if isempty(snr_val) || isempty(snr_est) || snr_est < 0.02 || snr_val < 0.02
        continue;
    end
    n_cells = n_cells + 1;
    
    for ii = 1:length(modelnames)
        sql = ['SELECT * FROM NarfResults WHERE batch=' num2str(batch)];
        sql = [sql ' AND modelname="' modelnames{ii} '" AND cellid="' cellids{jj} '"'];
        r = mysql(sql);
        
        if length(v_test) ~= 1
            v_test = v_test(1);
            fprintf('WARNING!!!!');
        end
        if length(v_fit) ~= 1
            fprintf('WARNING!!!!');
            v_fit = v_fit(1);
        end
        
        if isempty(v_test.isolation)
            v_test.isolation = NaN;
        end
        
        if isempty(v_fit.isolation)
            v_fit.isolation = NaN;
        end
        
        % THEMPORARY HACK: Remove an outlier
        %if v_fit.reps == 1 && v_test.reps == 20
        %     continue;
        % end
        %
        %             % THEMPORARY HACK: Remove outliers
        %if (v_fit.reps * snr_est < 0.4)
        %    continue;
        %end
        
        if ~isempty(snr_val) && ~isempty(snr_est)
            fitdata(ii,jj,7) = snr_est;
            testdata(ii,jj,7) = snr_val;
            fitdata(ii,jj,8) = z_est;
            testdata(ii,jj,8) = z_val;
            fitdata(ii,jj,9) = sec_est;
            testdata(ii,jj,9) = sec_val;
        end
        
        fitdata(ii,jj,1) = v_fit.reps;
        fitdata(ii,jj,2) = v_fit.spikes;
        fitdata(ii,jj,3) = v_fit.isolation;
        fitdata(ii,jj,4) = v_fit.spikes / v_fit.reps;
        
        if ~isempty(r)
            fitdata(ii,jj,5) = r(1).r_test / r(1).r_ceiling;
            fitdata(ii,jj,6) = r(1).r_fit.^2;
        end
        
        testdata(ii,jj,1) = v_test.reps;
        testdata(ii,jj,2) = v_test.spikes;
        testdata(ii,jj,3) = v_test.isolation;
        testdata(ii,jj,4) = v_test.spikes / v_test.reps;
        
        if ~isempty(r)
            testdata(ii,jj,5) = r(1).r_test / r(1).r_ceiling;
            %testdata(ii,jj,6) = r(1).r_test .^2;
            testdata(ii,jj,6) = r(1).r_ceiling .^2;
        end
        
    end
end

%testdata = reshape(testdata, [], 9);
%fitdata = reshape(fitdata, [], 9);

%testdata = excise(testdata);
%fitdata = excise(fitdata);

    function plotit(x, y, color)
        hold on
        %plot(x, y, 'k.');
        d = sortrows([x y]);
        gf = gausswin(2*ceil(length(x)/10));
        gf = gf / sum(gf);
        %plot(iconv(d(:, 1), gf), iconv(d(:, 2), gf), 'r-');        
        %p = polyfit(x,y,1);title('FitSNR vs TestSNR')
        %plot(x, p(1)*x + p(2), 'g-');
        
        rr = corr(x,y);
        % textLoc(sprintf('r=%f', rr), 'North');
        
        DD = [x' y'];
        DD = excise(DD);
        DD=DD(DD(:,1)>0 & DD(:,2)>0,:);
        x = DD(:, 1); 
        y = DD(:, 2);
        ix = 1./x;
        iy = 1./y;
        p = polyfit(ix,iy,1); 
        tt = linspace(0,max(x),500);
        plot(tt, 1./(p(1).*(1./tt) + p(2)), 'Color', color);
        hold off;
    end

    function plotit2(x, y, color)
        hold on
        %plot(x, y, 'k.');
        d = sortrows([x y]);
        gf = gausswin(2*ceil(length(x)/10));
        gf = gf / sum(gf);
        %plot(iconv(d(:, 1), gf), iconv(d(:, 2), gf), 'r-');        
        %p = polyfit(x,y,1);title('FitSNR vs TestSNR')
        %plot(x, p(1)*x + p(2), 'g-');
        
        rr = corr(x,y);
        % textLoc(sprintf('r=%f', rr), 'North');
        
        DD = [x' y'];
        DD = excise(DD);
        DD=DD(DD(:,1)>0 & DD(:,2)>0,:);
        x = DD(:, 1); 
        y = DD(:, 2);
        ix = 1./x;
        iy = 1./y;
        p = polyfit(ix,iy,1); 
        tt = linspace(0,max(ix),500);
        plot(tt, p(1).*tt + p(2), 'Color', color);
        hold off;
    end

%%%%%%%%%%%%%%%

figure;
for ii = 1:length(modelnames)
    plotit(fitdata(ii, :, 7).^2 .* fitdata(ii, :,1), testdata(ii, :, 6), pickcolor(ii));
    xlabel('(FitSNR)^2 * FitReps vs Rtest^2');
    ylabel('R_{ceiling}^2');    
end
legend(modelnames, 'Interpreter', 'none');

figure;
for ii = 1:length(modelnames)
    plotit2(fitdata(ii, :, 7).^2 .* fitdata(ii, :,1), testdata(ii, :, 6), pickcolor(ii));
    xlabel('1/[(FitSNR).^2 * FitReps vs Rtest^2]');
    ylabel('1/R_{ceiling}^2');    
end
legend(modelnames, 'Interpreter', 'none');


end
