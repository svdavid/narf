function plot_parameter_histograms_by_region(batch, cellids, modelnames)

modelnames = {'fb18ch100_lognn_wcg03_ap3z1_dexp_fit09c'};

batches = {267, 266, 264}; % HACKY. There must be 3 of these right now!

% JL June 2014: added this function to get the names in addition to the
% values (mostly copy+paste from pack_fittable.m)
    function w_names = pack_fittables_and_names(~)
        global STACK;
        w = [];
        names = {};
        w_names = cell(2,1);
        for iii = 1:length(STACK)
            mm = STACK{iii};
            nsplits = length(mm);
            for kk = 1:nsplits
                m = mm{kk};
                if isfield(m, 'fit_fields')
                    for jj = 1:length(m.fit_fields),
                        p = m.fit_fields{jj};
                        for ll = 1:numel(m.(p))
                            names = {names{:} [p sprintf('[%d]',ll)]};
                        end
                        w = cat(1, w, reshape(m.(p), numel(m.(p)), 1));
                    end
                end
            end
        end
        w_names{1} = w;
        w_names{2} = names;
    end

% stack_extractor = @pack_fittables;
stack_extractor = @pack_fittables_and_names;

pp = {};

for bb = 1:length(batches)
    batch = batches{bb};   
    sql = ['SELECT distinct cellid from sRunData WHERE batch=', num2str(batch)];
	ret = mysql(sql);
    cellids = {ret.cellid};
    
    [params, ~, ~] = load_model_batch(batch, cellids, modelnames, ...
        stack_extractor);
    
    % Check that the number of free parameters is the same in all models
    n_params = [];
    for ii = 1:numel(params{1})
        prms = params{1}{ii};
        if isempty(n_params)
            n_params = length(prms);
        else
            if isempty(prms)
                continue;
            end
            if n_params ~= length(prms)
                error(['plot_parameter_histograms.m requires all models ' ...
                    'to have the same number of free parameters.']);
            end
        end
    end
    
    for ii = numel(params):-1:1
        if isempty(params{ii})
            params(ii) = [];
            fprintf('excluding empty model %d\n',ii)
        end            
    end
    
    params2 = {};
    for jj = 1:length(cellids)
        [snr_val,snr_est, z_val, z_est, sec_val, sec_est]=db_get_snr(cellids{jj},batch);  
        
        if isempty(snr_val) && isempty(snr_est)
            continue;
        end
        
        if (snr_val < 0.02 || snr_est < 0.02)
            fprintf('Excluding cell %s because of SNR < 0.02\n', cellids{jj});
            continue;
        end
        
        params2{jj} = params{jj};
    end
        
    pp{bb} =  params2(cellfun(@(a) ~isempty(a), params2));
end

figure('Name', ['Parameter Histograms of ' cell2mat(modelnames(1))], 'NumberTitle', 'off', 'Position', [20 50 900 900]);


% warning('off','all');
% try
%     dat = cellfun(@(c) mat2cell(c{1}), params);
%     csvwrite('/tmp/parameter_histogram.csv', dat);
% catch whatever
%     fprintf('could not coexerce data to export to csv (cause: %s)\n', getReport(whatever));
% end
% warning('on','all');

% % This generic routine works for ANY model
% n = ceil(sqrt(n_params));
% for ii = 1:n_params
%     subplot(n, n, ii);
%     dd = nan(length(batches), 500);
%     for bb = 1:length(batches)
%         tmp = cellfun(@(c) c{1}(ii), pp{bb});
%         dd(bb,1:length(tmp)) = tmp(:); 
%     end
%     % Scale the distributions  
%     [NN, CC] = hist(dd', 20);    
%     NN = NN ./ repmat(nansum(NN), 20, 1);
%         
%     bar(CC, NN, 'Edgecolor', 'none');
%     unique_names = char(unique(cellfun(@(c) c{2}(ii), params))');    
%     xlabel(sprintf('%s [%3.2f to %3.2f]', unique_names, min(dd(:)), max(dd(:))));
%     
% end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% The following is specific for the modelname defined at the top of this file

% Set everything to NAN
gains = nan(length(batches), 500);
centers = gains;
bandwidths = gains;
poles1 = gains;
poles2 = gains;
poles3 = gains;
zeros = gains;
delays = gains;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Sort channels by their GAIN STRENGTH
zz = {};
for bb = 1:length(batches)
    tmp = [];
    tmp(:,1) = cellfun(@(c) c{1}(8), pp{bb});
    tmp(:,2) = cellfun(@(c) c{1}(9), pp{bb});
    tmp(:,3) = cellfun(@(c) c{1}(10), pp{bb});
    tmp2 = abs(tmp); 
    [~, z] = sort(tmp2, 2, 'Descend');    
    zz{bb} = z(:,1);
    
    % Normalize gains by relative strength
    tmp = tmp2 ./ repmat(nansum(tmp2, 2)', 3, 1)';
    
    for zi = 1:length(zz{bb}(:,1))
        gains(bb, zi) = tmp(zi, zz{bb}(zi,1));
    end
end

subplot(3, 3, 1);
[NN, CC] = hist(gains', linspace(0, 1, 20));
NN = NN ./ repmat(nansum(NN), 20, 1);
plot(CC, NN);    
xlabel('Dominant Channel L1-Norm Relative Magnitude');
n1 = sum(~isnan(zz{1}(:))');
n2 = sum(~isnan(zz{2}(:))');
n3 = sum(~isnan(zz{3}(:))');
legend(sprintf('267(A1 N=%d) [%f]', n1, nanmean(gains(1, :)')), ...
       sprintf('266(PEG N=%d) [%f]', n2, nanmean(gains(2, :)')), ...
       sprintf('264(PPF N=%d) [%f]', n3, nanmean(gains(3, :)')));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CENTER FREQS
for bb = 1:length(batches)
    tmp = [];
    tmp(:,1) = cellfun(@(c) c{1}(2), pp{bb});
    tmp(:,2) = cellfun(@(c) c{1}(4), pp{bb});
    tmp(:,3) = cellfun(@(c) c{1}(6), pp{bb});    
    for zi = 1:length(zz{bb}(:,1))
        centers(bb, zi) = abs(tmp(zi,zz{bb}(zi,1)));
    end
end    

subplot(3, 3, 2);
[NN, CC] = hist(centers', linspace(0, 20, 20));
NN = NN ./ repmat(nansum(NN), 20, 1);
plot(CC, NN);    
xlabel('Dominant Channel Center Freq [kHz]');
legend(sprintf('267(A1) [%f]', nanmean(centers(1, :)')), ...
       sprintf('266(PEG) [%f]', nanmean(centers(2, :)')), ...
       sprintf('264(PPF) [%f]', nanmean(centers(3, :)')));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Bandwidths
for bb = 1:length(batches)
    tmp = [];
    tmp(:,1) = cellfun(@(c) c{1}(3), pp{bb});
    tmp(:,2) = cellfun(@(c) c{1}(5), pp{bb});
    tmp(:,3) = cellfun(@(c) c{1}(7), pp{bb});   
    for zi = 1:length(zz{bb}(:,1))
        bandwidths(bb, zi) = abs(tmp(zi,zz{bb}(zi,1)));
    end
end

subplot(3, 3, 3);
[NN, CC] = hist(bandwidths', linspace(0, 20, 20));
NN = NN ./ repmat(nansum(NN), 20, 1);
plot(CC, NN);    
xlabel('Dominant Channel Bandwidth [Not Octaves]');
legend(sprintf('267(A1) [%f]', nanmean(bandwidths(1, :)')), ...
       sprintf('266(PEG) [%f]', nanmean(bandwidths(2, :)')), ...
       sprintf('264(PPF) [%f]', nanmean(bandwidths(3, :)')));
    

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Poles 11-19
for bb = 1:length(batches)
    tmp = [];
    tmp(:,1,1) = cellfun(@(c) c{1}(11), pp{bb});
    tmp(:,2,1) = cellfun(@(c) c{1}(12), pp{bb});
    tmp(:,3,1) = cellfun(@(c) c{1}(13), pp{bb});
    tmp(:,1,2) = cellfun(@(c) c{1}(14), pp{bb});
    tmp(:,2,2) = cellfun(@(c) c{1}(15), pp{bb});
    tmp(:,3,2) = cellfun(@(c) c{1}(16), pp{bb});
    tmp(:,1,3) = cellfun(@(c) c{1}(17), pp{bb});
    tmp(:,2,3) = cellfun(@(c) c{1}(18), pp{bb});
    tmp(:,3,3) = cellfun(@(c) c{1}(19), pp{bb});
        
    for zi = 1:length(zz{bb}(:,1))
        poles_sorted = sort(tmp(zi, zz{bb}(zi,1), :), 'Descend');              
        poles1(bb, zi) = 1000./abs(poles_sorted(1));        
        poles2(bb, zi) = 1000./abs(poles_sorted(2));
        poles3(bb, zi) = 1000./abs(poles_sorted(3));
        
        if poles1(bb,zi) > 300
            poles1(bb,zi) = nan;
        end
        if poles2(bb,zi) > 300
            poles2(bb,zi) = nan;
        end
        if poles3(bb,zi) > 300
            poles3(bb,zi) = nan;
        end
        
    end
end

subplot(3, 3, 4);
[NN, CC] = hist(poles1', linspace(0, 60, 20));
NN = NN ./ repmat(nansum(NN), 20, 1);
plot(CC, NN);    
xlabel('Dominant Channel Slowest Pole Decay Constant [ms]');
legend(sprintf('267(A1) [%f]', nanmean(poles1(1, :)')), ...
       sprintf('266(PEG) [%f]', nanmean(poles1(2, :)')), ...
       sprintf('264(PPF) [%f]', nanmean(poles1(3, :)')));
   

subplot(3, 3, 5);
[NN, CC] = hist(poles2', linspace(0, 40, 20));
NN = NN ./ repmat(nansum(NN), 20, 1);
plot(CC, NN);    
xlabel('Dominant Channel Middle Pole Decay Constant [ms]');
legend(sprintf('267(A1) [%f]', nanmean(poles2(1, :)')), ...
       sprintf('266(PEG) [%f]', nanmean(poles2(2, :)')), ...
       sprintf('264(PPF) [%f]', nanmean(poles2(3, :)')));

subplot(3, 3, 6);
[NN, CC] = hist(poles3', linspace(0, 30, 20));
NN = NN ./ repmat(nansum(NN), 20, 1);
plot(CC, NN);    
xlabel('Dominant Channel Fastest Pole Decay Constant [ms]');
legend(sprintf('267(A1) [%f]', nanmean(poles3(1, :)')), ...
       sprintf('266(PEG) [%f]', nanmean(poles3(2, :)')), ...
       sprintf('264(PPF) [%f]', nanmean(poles3(3, :)')));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% zeros 20-22
for bb = 1:length(batches)
    tmp = [];
    tmp(:,1) = cellfun(@(c) c{1}(20), pp{bb});
    tmp(:,2) = cellfun(@(c) c{1}(21), pp{bb});
    tmp(:,3) = cellfun(@(c) c{1}(22), pp{bb});   
    for zi = 1:length(zz{bb}(:,1))
        zeros(bb, zi) = 1000./abs(tmp(zi,zz{bb}(zi,1)));
        if zeros(bb,zi) > 300
            zeros(bb,zi) = nan;
        end
        
    end
end

subplot(3, 3, 7);
[NN, CC] = hist(zeros', linspace(0, 300, 20));
NN = NN ./ repmat(nansum(NN), 20, 1);
plot(CC, NN);    
xlabel('Dominant Channel Zero Decay Constant [ms]');

legend(sprintf('267(A1) [%f]', nanmean(zeros(1, :)')), ...
       sprintf('266(PEG) [%f]', nanmean(zeros(2, :)')), ...
       sprintf('264(PPF) [%f]', nanmean(zeros(3, :)')));
   
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% delays 23-25
for bb = 1:length(batches)
    tmp = [];
    tmp(:,1) = cellfun(@(c) c{1}(23), pp{bb});
    tmp(:,2) = cellfun(@(c) c{1}(24), pp{bb});
    tmp(:,3) = cellfun(@(c) c{1}(25), pp{bb});    
    tmp = abs(tmp);
    for zi = 1:length(zz{bb}(:,1))
        delays(bb, zi) = tmp(zi,zz{bb}(zi,1));
    end
end

subplot(3, 3, 8);
[NN, CC] = hist(delays', linspace(min(delays(:)), max(delays(:)), 20));
NN = NN ./ repmat(nansum(NN), 20, 1);
plot(CC, NN);    
xlabel('Dominant Channel Latency [ms]');
legend(sprintf('267(A1) [%f]', nanmean(delays(1, :)')), ...
       sprintf('266(PEG) [%f]', nanmean(delays(2, :)')), ...
       sprintf('264(PPF) [%f]', nanmean(delays(3, :)')));
   

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Plotting pole 1 vs pole2 yielded no insight.
% Maybe plotting BF vs bandwidth does?

subplot(3, 3, 9);
hold on;
tt = tmp(1,:,:)
%[NN, CC] = hist(tt, linspace(-300, 0, 20));
%plot(CC, NN, 'b-');    
%[NN, CC] = hist(tt, linspace(-300, 0, 20));
%plot(CC, NN, 'g-');    
%[NN, CC] = hist(tt, linspace(-300, 0, 20));
%plot(CC, NN, 'r-');    
plot(centers(:), bandwidths(:), 'k.');
xlabel('Center Freq [kHz]');
ylabel('Bandwidth [Not Octaves]');
legend('267(A1)', '266(PEG)', '264(PPF)');
hold off;

end
