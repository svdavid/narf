% function [mXC,eXC]=spike_train_similarity(r,binsize);
%
% mXC(1): correlation between repetitions versus trials
% mXC(2): correlation between trials with different stimuli
%
%
function [mXC,eXC]=spike_train_similarity(r,binsize);

repcount=size(r,2);
stimcount=size(r,3);

xcN=200;
DOCORR=0;

if ~exist('binsize','var'),
    binsize=1;
end
if binsize>1,
    smfilt=ones(binsize,1)./binsize;
    rnan=find(isnan(r));
    r(rnan)=nanmean(r(:));
    r(:,:)=conv2(r(:,:),smfilt,'same');
    r(rnan)=nan;
    r=r(round(binsize./2):binsize:end,:,:);
end

mXC=ones(2,1);
eXC=ones(2,1);
    
tmsein=[];
tmseout=[];
for s1=1:stimcount,
    fprintf('.');
    for t1=1:repcount-1,
        r1=r(:,t1,s1);
        r2=r(:,(t1+1):end,s1);
        rnorm=nanstd(r1).*sqrt(2);
        for ri=1:size(r2,2),
            rnorm2=nanstd(r2(:,ri));
            if rnorm && rnorm2,
                if DOCORR,
                    x=xcorr(r1,r2(:,ri),0,'coeff');
                else
                    x=xcov(r1,r2(:,ri),0,'coeff');
                end
                tmsein=cat(2,tmsein,x);
            else
                tmsein=cat(2,tmsein,0);
            end
        end
    end
    for t1=1:repcount,
        for s2=[1:(s1-1) (s1+1):stimcount],
            r1=r(:,t1,s1);
            r2=r(:,:,s2);
            rnorm=nanstd(r1);
            for ri=1:size(r2,2),
                rnorm2=nanstd(r2(:,ri));
                if rnorm && rnorm2,
                    if DOCORR,
                        x=xcorr(r1,r2(:,ri),0,'coeff');
                    else
                        x=xcov(r1,r2(:,ri),0,'coeff');
                    end
                    tmseout=cat(2,tmseout,x);
                else
                    tmseout=cat(2,tmseout,0);
                end
            end
        end
    end
end

[mXC(1),eXC(1)]=jackmeanerr(tmsein');
[mXC(2),eXC(2)]=jackmeanerr(tmseout');

fprintf(' XC=%.3f+/-%.3f (intertrial=%.3f+/-%.3f)',...
        mXC(1),eXC(1),...
        mXC(2),eXC(2));
