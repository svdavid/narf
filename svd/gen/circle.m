function [x,y]=circle(x0,y0,r,n);

if not(exist('n')),
   n=21;
end

x=zeros(n,1);
y=x;

for rr=1:n,
   theta=2*pi/(n-1)*(rr-1);
   x(rr)=r*cos(theta)+x0;
   y(rr)=r*sin(theta)+y0;
end

