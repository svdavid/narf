% function r=raw_nl2(beta,pred);
%
function r=raw_nl2(beta,pred);

pp=beta{1};
rr=beta{2};
if length(beta)>2,
    v=beta{3};
    pred=pred*v;
end

if isempty(pp),
    warning('empty beta, returning linear prediction');
    r=pred;
    return
end

bincount=size(pp);
tcount=size(pred,1);
r=zeros(tcount,1);

if std(pp(:))>0 && std(rr(:))>0,
   xx=pp;
   xx=[xx(1,:)-(xx(2,:)-xx(1,:))*5;
      xx;
      xx(end,:)+(xx(end,:)-xx(end-1,:))*5];
   yy=rr;
   yy=[yy(1,:)-(yy(2,:)-yy(1,:))*5;
      yy;
      yy(end,:)+(yy(end,:)-yy(end-1,:))*5];
  
   yy=[yy(:,1)-(yy(:,2)-yy(:,1))*5 ...
      yy ...
      yy(:,end)+(yy(:,end)-yy(:,end-1))*5];

   % Jean: commented that, it is not useful here
%    inrange=find(pred(:,1)>=xx(1,1) & pred(:,1)<xx(end,1) &...
%       pred(:,2)>=xx(1,2) & pred(:,2)<xx(end,2));
      
   %I = INTERP1(X,Y,XI,'method','extrap')
   %if sum(diff(xx)==0)>0,
   %    xx=xx+eps.*(1:length(xx))';
   %end
%    ff=find(diff(round(xx./max(xx(:)).*1000000))==0);
%    if ~isempty(ff),
%       keepidx=setdiff(1:length(xx),ff+1);
%       % Jean: was broken, fixed it
%       % before:
% %       xx=xx(keepidx);
% %       yy=yy(keepidx);
%       xx=xx(keepidx,:);
%       yy=yy(keepidx,keepidx);
%    end

   doubled_values_x1 = ((xx(2:end,1) - xx(1:(end-1),1)) < 1e-6);
   if sum( doubled_values_x1 )
%        xx(:,1) = linspace( xx(1,1)-1e-6, xx(end,1)+1e-6, size(xx,1)); % enforce different values for interpolation
        xx = xx([true ~doubled_values_x1'],:);
   end
   
   if size(xx,1) > 1
       doubled_values_x2 = ((xx(2:end,2) - xx(1:(end-1),2)) < 1e-6);
       if sum( doubled_values_x2 )
           %        xx(:,2) = linspace( xx(1,2)-1e-6, xx(end,2)+1e-6, size(xx,2)); % enforce different values for interpolation
           xx = xx([true ~doubled_values_x2'],:);
       end
   end
   
   if size(xx,1) ~= size(yy,1)
       % the only way we are in this case is when two or more the values
       % from input1 (or input2) are exactly the same.
       % Here, we create a fake interpolation matrix to avoid a bug in interp2.
       % Later on the fitter should realize that the prediction is constant
       if (size(xx,1) == 1)
           xx = [xx-1;xx;xx+1]; % hacky way to generate different values to not have a matrix filled with the same value (which breaks interp1)
       end
       xx1 = interp1(xx(:,1), linspace(1,size(xx,1),size(yy,1)) );
       xx2 = interp1(xx(:,2), linspace(1,size(xx,1),size(yy,1)) );
       xx = [xx1; xx2]'; 
   end

   [x1,x2] = meshgrid(xx(:,1),xx(:,2));

   try
   r=interp2(x1,x2,yy,pred(:,1),pred(:,2),'linear');
   catch Err
        fprintf('\nWarning: bug in raw_nl2.m (caused by NaN?)\n ===> proceeding by taking the raw values from stim 1 as predictions\n');
        r=pp(:,1);
   end

end

