function [snr_val,snr_est, z_val, z_est, seconds_val, seconds_est]=db_get_snr(cellid, batch, batchdata)
% function [snr_val,snr_est, z_val, z_est, seconds_val, seconds_est]=db_get_snr(cellid, batch, batchdata)

dbopen;
if ~exist('batchdata', 'var')
    sql=['SELECT * FROM NarfBatches WHERE cellid="',cellid,'"',...
         ' AND batch=',num2str(batch)];
    batchdata=mysql(sql);

    if isempty(batchdata),
        error('cellid/batch not found in NarfBatches');
    end
end

val_set=char(batchdata.val_set);
val_set=strrep(val_set,'{','(');
val_set=strrep(val_set,',}',')');
val_set=strrep(val_set,'_val''','''');

%sql=['SELECT respSNR,respZ FROM sCellFile where cellid="',cellid,'"',...
%    ' AND stimfile in ',val_set];
sql=['SELECT * FROM sCellFile where cellid="',cellid,'"',...
     ' AND stimfile in ', val_set];
edata=mysql(sql);
if isempty(edata)
    fprintf('Whaaaaaa!?\n');
    Spikefs = 25000;
else
    ed = edata(1);
    evpfile=[ed.stimpath ed.stimfile '.evp'];
    try
        [~,~,~,Spikefs]=evpgetinfo(evpfile, 1);
    catch
        fprintf('WARNING: Defaulting to 25kHz sampling rate\n');
        Spikefs=25000;
    end
end

snr_val=max(cat(1,edata.respSNR));
z_val=max(cat(1,edata.respZ));
seconds_val=sum(cat(1, edata.resplen))./Spikefs;

if nargout>1,
    % only execute if second parameter requested
    est_set=char(batchdata.est_set);
    est_set=strrep(est_set,'{','(');
    est_set=strrep(est_set,',}',')');
    est_set=strrep(est_set,'_est''','''');
    %sql=['SELECT respSNR,respZ FROM sCellFile where cellid="',cellid,'"',...
    %     ' AND stimfile in ',est_set];
    sql=['SELECT * FROM sCellFile where cellid="',cellid,'"',...
         ' AND stimfile in ',est_set];
    edata=mysql(sql);
    snr_est=max(cat(1,edata.respSNR));
    z_est=max(cat(1,edata.respZ));
    seconds_est=sum(cat(1, edata.resplen))./Spikefs;
end

