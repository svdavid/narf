function coefs = fir_gaussian(phi, N)
    [N_chans, N_parms] = size(phi);
    
    if N_parms ~= 3
        error('fir_gaussian needs exactly 3 parameters per channel');
    end
    
    delay = phi(:,1); 
    sigma = abs(phi(:,2));
    amp = phi(:,3);   
    
    coefs = zeros(N);
    
    if ~any(sigma < 0)    
        t=0:(N(2)-1);
        for c = 1:N_chans
            coefs(c,:) = amp(c) .* gauss1([delay(c) sigma(c)], t);
        end
    end
            
end
