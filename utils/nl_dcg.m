%function ret = nl_dcg(phi, z)
%
% not really a nonlinearity but can be slotted into various places
% for testing behavior effects.
%
% phi [offset gain] default gain=1.
%
%svd
function ret = nl_dcg(phi, z)
    if length(phi)==1,
        ret = (z - phi(1));
    elseif length(phi)==2,
        ret = (z - phi(1)).*phi(2);
    else
        % if length phi==3, special code to ignore dc term
        ret = z.*phi(2);
    end
end
