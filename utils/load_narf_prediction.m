%function [pred,resp,fs,perf]=load_narf_prediction(cellid,modelname,batch,stimfile)
%
% load model parameters, generate and return predicted response to stimfile
%
function [pred,resp,fs,perf]=load_narf_prediction(cellid,modelname,batch,stimfile)

global STACK META XXX

cellids={cellid};
modelnames={modelname};
[stacks,metas,x0s,perf]=load_model_batch(batch,cellids,{modelname});

ii=1;
STACK=stacks{ii};
XXX={x0s{ii}};
files=cat(2,XXX{1}.training_set,XXX{1}.test_set);

if ~exist('stimfile','var'),

   stimfile=XXX{1}.test_set{1};
   fprintf('defaulting to stimfile=%s\n',stimfile);
end

if sum(strcmp(stimfile,files)),
   s2=stimfile;
elseif sum(strcmp(files,[stimfile,'_est'])),
   XXX{1}.test_set={stimfile};
   s2=stimfile;
end
update_xxx(2);
fs=STACK{1}{1}.raw_resp_fs;
resp=XXX{end}.dat.(s2).resp.*fs;
pred=XXX{end}.dat.(s2).stim;
perf=perf{ii};

