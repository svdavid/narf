function ret = nl_softzero(phi, z)
    alpha = phi(1);  % Scaling parameter
    beta  = phi(2);  % Curvature
    theta = phi(3);  % Offset   
    
    if (length(phi) > 3)
        yoffset = phi(4);  % Offset   
    else
        yoffset = 0;
    end
    bz=beta*(z-theta);
    loz=find(bz<10);
    
    ret = alpha*(z-theta) + yoffset; 
    ret(loz) = alpha./beta * log(1 + exp(bz(loz))) + yoffset; 
end
