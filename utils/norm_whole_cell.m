function rnorm=norm_whole_cell(r,SR);
%function rnorm=norm_whole_cell(r,SR);
%
% r=current trace (sign already flipped for E current!)
% SR=sampling rate
%
% rnorm= r with baseline modulations subtracted
%
    
    N=round(SR./2);
    
    % smooth the raw trace
    sy=gsmooth(r(:),N./2);
    
    % find local minima
    y=zeros(size(sy(:)));
    for ii=1:length(r(:));
        xx=max(1,ii-N);
        yy=min(length(sy(:)),ii+N);
        y(ii)=min(sy(xx:yy));
    end
    
    % smooth the minima
    y=gsmooth(y,N./2);
    
    figure;
    subplot(2,1,1);
    plot([r(:) sy r(:)-y y]);
    axis tight
    subplot(2,1,2);
    ty=r(:)-y;
    ts=nanmean(si,2);
    plot([ts./max(ts) ty./max(ty)]);
    axis tight
    
    % subtract minima
    r=r(:)-y;
    

    
% function gx=gsmooth(x,sigma,boundcond,offset)
%
% gaussian smoothing along first non-singleton dimension of x.
% filter has std dev equal to sigma (default 1)
% if sigma=[srow scol], smooth with 2D gaussian, srows across rows
%
% boundcond (default=0) 0-return gx same length as x
%                       1-return gx with tails
%                       2-return valid region of gx only
%
function gx=gsmooth(x,sigma,boundcond,offset)

if ~exist('sigma','var'),
   sigma=1;
end
if ~exist('boundcond','var'),
   boundcond=0;
end
if ~exist('offset','var'),
   offset=0;
end

%fprintf('gsmooth(...,%.2f,%d,%d):\n',sigma(1),boundcond,offset);

if abs(offset)>0,
   ll=size(x,2);
   x=[x fliplr(x(:,(ll-offset+1):ll))];
end
if length(sigma)==1,
   tt=(floor(-sigma*4):ceil(sigma*4));
   pfilt=exp(-tt.^2/(2*sigma.^2))./(sqrt(2*pi)*sigma);
   pfilt=pfilt./sum(pfilt);
   if size(x,1)>1,
      pfilt=pfilt';
   end
else
   [xx,yy]=meshgrid(floor(-sigma(2)*4):ceil(sigma(2)*4),...
                    floor(-sigma(1)*4):ceil(sigma(1)*4));
   pfilt=exp(-xx.^2/(2*sigma(2).^2)-yy.^2/(2*sigma(1).^2));
   pfilt=pfilt./sum(pfilt(:));
end
   
if boundcond==0,
   gx=rconv2(x,pfilt);
elseif boundcond==1,
   gx=conv2(x,pfilt,'full');
elseif boundcond==2,
   gx=conv2(x,pfilt,'valid');
end

if abs(offset)>0,
   gx=gx(:,(offset+1):end);
end

% RES = RCONV2(MTX1, MTX2, CTR)
%
% Convolution of two matrices, with boundaries handled via reflection
% about the edge pixels.  Result will be of size of LARGER matrix.
% 
% The origin of the smaller matrix is assumed to be its center.
% For even dimensions, the origin is determined by the CTR (optional) 
% argument:
%      CTR   origin
%       0     DIM/2      (default)
%       1     (DIM/2)+1  

% Eero Simoncelli, 6/96.

function c = rconv2(a,b,ctr)

if (exist('ctr') ~= 1)
  ctr = 0;
end

if (( size(a,1) >= size(b,1) ) & ( size(a,2) >= size(b,2) ))
    large = a; small = b;
elseif  (( size(a,1) <= size(b,1) ) & ( size(a,2) <= size(b,2) ))
    large = b; small = a;
else
  error('one arg must be larger than the other in both dimensions!');
end

ly = size(large,1);
lx = size(large,2);
sy = size(small,1);
sx = size(small,2);

%% These values are one less than the index of the small mtx that falls on 
%% the border pixel of the large matrix when computing the first 
%% convolution response sample:
sy2 = floor((sy+ctr-1)/2);
sx2 = floor((sx+ctr-1)/2);

% pad with reflected copies
clarge = [ 
    large(sy-sy2:-1:2,sx-sx2:-1:2), large(sy-sy2:-1:2,:), ...
	large(sy-sy2:-1:2,lx-1:-1:lx-sx2); ...
    large(:,sx-sx2:-1:2),    large,   large(:,lx-1:-1:lx-sx2); ...
    large(ly-1:-1:ly-sy2,sx-sx2:-1:2), ...
      large(ly-1:-1:ly-sy2,:), ...
      large(ly-1:-1:ly-sy2,lx-1:-1:lx-sx2) ];

c = conv2(clarge,small,'valid');

