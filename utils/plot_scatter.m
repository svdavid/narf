function plot_scatter(X, names, plot_marginals, autoscale)
% plot_scatter(X, names, plot_marginals, autosale)
% Plots a big scatter chart of your data X.
% Each column of X is a different data set that you want to plot
% NAMES is a cell array of strings naming each column
% If you want to plot marginals or use auto-scaling, set PLOT_MARGINALS or
% AUTOSCALE to true.

% 2012/10/03, Ivar Thorson.

if ~exist('plot_marginals', 'var')
    plot_marginals = false;
end
if ~exist('autoscale', 'var')
    autoscale = false;
end

X = excise(X);

% Set plot borders and sizes
tmargin = 0.1;
lmargin = 0.1;
rmargin = 0.1;
bmargin = 0.1;

% Set text position on each subplot, from 0 to 1
textx = 0.05;  % 0.1=Left
texty = 0.95;  % 0.9=Top

axmin = -0.2;
axmax = 0.9;
axmin = min(min(X(:)), 0);
axmax = max(X(:));

bins = linspace(axmin, axmax, 20);

[Npoints, Nsets] = size(X);

if not(exist('names')) || length(names) ~= Nsets
    error('I need as many names as the size of the y dimension!');
end

names = shorten_modelnames(names);

R = corrcoef(X);

w = (1.0 - lmargin - rmargin) / (Nsets - 1);    % Plot width
h = (1.0 - tmargin - bmargin) / (Nsets - 1);    % Plot height

for i = 1:Nsets
    for j = 1:Nsets;
        
        % Calculate draw location
        if i < j
            subplot('Position', [(lmargin+(i-1)*w) (bmargin + (Nsets-j)*h) w h]);
        else
            continue;
        end
        
        if ~autoscale
            axis([axmin, axmax, axmin, axmax]);
        else
            % Use the 5-95% range for plotting data (ignore outliers)
            xs = sort(X(:,i));  
            ys = sort(X(:,j));
            bot = ceil(size(X,1)*0.05);
            top = ceil(size(X,1)*0.95);
            %axis([xs(bot), xs(top), ys(bot), ys(top)]);
        end
        
        hold on;
        if j == Nsets && plot_marginals
             qtys = hist(X(:,i), bins);
             bar(bins, (qtys ./ sum(qtys)), 'g');
        end

        if i == 1 && plot_marginals
             qtys = hist(X(:,j), bins);
             bh = barh(bins, (qtys ./ sum(qtys)), 'y');
        end

        plot(X(:,i),X(:,j), 'k.');
        
        if ~autoscale
            plot([axmin, axmax], [axmin, axmax], 'k--');
        else
            xx = X(:,i);
            yy = X(:,j);
            %xcrop = xx(xx>xs(bot) & xx<xs(top));     
            %ycrop = yy(yy>ys(bot) & yy<ys(top));            
            p = polyfit(xx,yy,2);   
            t = min(xx):0.01:max(xx);
            plot(t, p(1).*t.^2 + p(2).*t + p(3), 'r--');             
            textLoc(sprintf('y = %0.3fx^2 + %0.3fx + %0.3f\n', p(1), p(2), p(3)), 'NorthEast');
            pearson = corrcoef(xx, yy);
            textLoc(sprintf('Corr = %0.3f\n', pearson(2,1)), 'NorthWest');
        end
        
        % Plot the CDFs versus each other, like a Kolmogorov Smirnov plot
%         if ~all(isnan(X(:,i))) && ~all(isnan(X(:,j)))
%             [cdfi, i_values] = ecdf(X(:,i));
%             [cdfj, j_values] = ecdf(X(:,j));
%             grid = linspace(axmin, axmax, 100);
%             [~, iix] = unique(i_values);
%             [~, ijx] = unique(j_values);
%             ipts = interp1(i_values(iix), cdfi(iix), grid, 'linear');
%             jpts = interp1(j_values(ijx), cdfj(ijx), grid, 'linear');
%             plot(jpts, ipts, 'r-');
%         end
                      
        % Turn off tick labels unless in the bottom or leftmost rows
        if j == Nsets 
            hl = xlabel(sprintf('%s\n\nmean:%.3f\nmed:%.3f', names{i}, ...
                                nanmean(X(:,i)),nanmedian(X(:,i))));
            set(hl,'interpreter','none');
        else
            set(gca, 'XTickLabel', '');
        end
        
        if i == 1 
            hl = ylabel(sprintf('%s\n\nmean:%.3f\nmed:%.3f', names{j}, ...
                                nanmean(X(:,j)),nanmedian(X(:,j))));
            set(hl,'interpreter','none');
        else
            set(gca, 'YTickLabel', '');
        end
        
        hold off;
    end
end
