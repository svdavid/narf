function ax = plot_perf_vs_truncation(data, modelnames, metric_name)
% A plot of model performance vs the amount of data that has been truncated
        
if ~any(strcmp(metric_name, {'r_ceiling', 'r_test', 'r_fit', 'r_test - r_fit'}))
    error('I only understand correlation metrics right now!');
end
    
% Compute the means and standard error of each model
d_nparm  = nanmean(data(:,:,1));  % Means should result in no change
D = data(:,:);
D(D==0) = NaN;
d_metric = D; %abs(D);

jcount=20;
jstep=size(D,1)./jcount;
djmeans=zeros(jcount,size(D,2));
%di=find(mean(d_metric)==max(mean(d_metric)));
di=1:5;
%di=1;
dd=d_metric./repmat(mean(d_metric(:,di),2),[1 size(d_metric,2)]);
dd=dd.*mean(mean(d_metric(:,di)));
for jj=1:jcount,
   ff=[1:round((jj-1)*jstep) round(jj*jstep+1):size(D,1)];
   djmeans(jj,:)=nanmean(dd(ff,:));
end

d_err = std(djmeans).*sqrt(jcount-1);

%d_means = sqrt(nanmean((d_metric.^2).*sign(d_metric)));
d_means = nanmean(d_metric);
%d_means = nanmedian(d_metric);
d_count = sum(~isnan(d_metric));
d_stddev = sqrt(nanvar(d_metric));
d_stderr = d_stddev ./ sqrt(d_count);
len = length(modelnames);

%---------------
% Create a structure to hold model names differing only by "trunc"
% Start by tokenizing and removing any "trunc" keywords
names = {};
for ii = 1:length(modelnames)
    tmp = tokenize_string(modelnames{ii});
    tmp = [tmp{:}];
    tmp = tmp( cellfun(@isempty, (strfind(tmp, 'trunc'))));
    mm = cellfun(@(x) [x '_'], tmp, 'UniformOutput', false);
    names{ii} = [mm{:}];
    names{ii} = names{ii}(1:end-1); % Trim off last _, if any    
end

% Create list of unique names
uncount=length(unique(names));
unames = names(1:uncount);

% Make "pretty" names with parameter count included
pnames = [];
n_parms = [];
for ii = 1:length(unames)
    sql = ['select * from NarfResults where modelname="' unames{ii} '";'];
    ret = mysql(sql);
    if ~isempty(ret)
        n_parms(ii) = ret(1).n_parms;        
        pnames{ii} = sprintf('[n=%d] %s', n_parms(ii), unames{ii});
    else
        pnames{ii} = unames{ii};
        n_parms(ii) = NaN;
    end
end

% Resort by the number of parameter count
%[~, idx] = sort(n_parms);
% don't re-sort
idx=1:length(n_parms);
unames = unames(idx);
pnames = pnames(idx);

scores = {};
jscores = {};
hl=zeros(length(unames),1);
for ii = 1:length(unames)    
    % Collect indexes that match each unique name
    idxs = strcmp(unames{ii}, names);      
    
    scores{ii} = [0, 0];
    jscores{ii} = zeros(1,jcount+1);
    for jj = 1:length(idxs)       
        idx = idxs(jj);
        if ~idx  % Skip false indexes
            continue;
        end
        % Get the truncation amount
        trunc = regexp(modelnames{jj}, 'trunc(\d+)', 'tokens');
        if isempty(trunc)
            trunc = 100;
        else
            trunc = str2double(trunc{1});
        end
        
        % Store it in a structure
        if ~isnan(d_means(jj))
            scores{ii} = cat(1, scores{ii}, [trunc d_means(jj)]);
            jscores{ii}=cat(1,jscores{ii},[trunc djmeans(:,jj)']);
        end
    end
    
    scores{ii} = sortrows(scores{ii});
    jscores{ii} = sortrows(jscores{ii});
end


rjset=zeros(length(unames),jcount);
jmean=zeros(size(jscores{1},1),length(unames));
jerr=zeros(size(jscores{1},1),length(unames));
for ii = 1:length(unames)
   for jj=1:jcount,
      ni=1./(jscores{ii}(2:end,1));
      ri=1./(jscores{ii}(2:end,jj+1)).^2.*sign(jscores{ii}(2:end,jj+1));
      ni=[ni;ni((end-3):end);ni((end-1):end)];
      ri=[ri;ri((end-3):end);ri((end-1):end)];
      p=polyfit(ni,ri,1);
      %x=linspace(0.009,max(ni).*1.1,100);
      %y=p(1)*x+p(2);
      rjset(ii,jj)=sqrt(1./p(2));
   end
   jmean(:,ii)=mean(jscores{ii}(:,2:end),2);
   jerr(:,ii)=std(jscores{ii}(:,2:end),0,2).*sqrt(jcount-1);
end
rmean=mean(rjset,2);
rerr=std(rjset,0,2).*sqrt(jcount-1);

clf;
lstr=cell(1,length(unames));
rset=zeros(length(unames),2);

for ii = 1:length(unames)   
    n=scores{ii}(2:end,1);
    r=scores{ii}(2:end,2);
    ni=1./(scores{ii}(2:end,1));
    ri=1./(scores{ii}(2:end,2)).^2.*sign(scores{ii}(2:end,2));
    ni=[ni;ni((end-3):end);ni((end-1):end)];
    ri=[ri;ri((end-3):end);ri((end-1):end)];
    p=polyfit(ni,ri,1);
    x=linspace(0.009,max(ni).*1.1,100);
    y=p(1)*x+p(2);
    
    subplot(2,2,1);
    plot(1./x,1./sqrt(y),'--','Color',pickcolor(ii), ...
         'MarkerFaceColor', pickcolor(ii));
    hold on;
    errorbar(120+randn*3,1./sqrt(p(2)),rerr(ii),'Marker','o',...
       'LineWidth', 2,'color',pickcolor(ii));
    %hl(ii)=plot(n,r, 'Marker', 'o', 'MarkerEdgeColor', 'k', ...
    %            'MarkerSize', 2, 'LineWidth', 2, 'color', pickcolor(ii), ...
    %            'LineStyle', pickline(1+floor(ii/12)));
    hl(ii)=errorbar(n,r,jerr(2:end,ii), ...
                'MarkerSize', 2, 'LineWidth', 2, 'color', pickcolor(ii), ...
                'LineStyle', pickline(1+floor(ii/12)));
    
    subplot(2,2,3);
    hl(ii)=plot(ni,ri, 'Marker', 'o', 'MarkerEdgeColor', 'k', ...
                'MarkerSize', 2, 'LineWidth', 2, 'color', pickcolor(ii), ...
                'LineStyle', pickline(1+floor(ii/12)));
    hold on;
    x=[0 max(ni)];
    plot(x,p(1)*x+p(2),'--','color', pickcolor(ii));
    mn=strrep(pnames{ii},'fb18ch100_lognn_','');
    mn=strrep(mn,'_',' ');
    mn=strrep(mn,'fit','');
    lstr{ii}=sprintf('%s rinf=%.3f r2=%.2f',...
                     mn,sqrt(1./p(2)),(1./p(2)));
    rset(ii,1)=scores{ii}(end,2);
    rset(ii,2)=sqrt(1./p(2));
    
end

subplot(2,2,1);
legend(hl,pnames, 'Location', 'SouthEast', 'Interpreter', 'none');
title('Model Performance vs Data Fraction');
xlabel('Percent of Estimation Set Data Used');
ylabel(sprintf('%s', metric_name), 'interpreter', 'none');
aa=axis;
axis([0 130 aa(3:4)]);
hold off;

subplot(2,2,3);
legend(hl,lstr, 'Location', 'SouthEast');
title('Model Performance vs Data Fraction');
xlabel('1/(Percent of Estimation Set Data Used)');
ylabel(sprintf('1/(%s^2)', metric_name), 'interpreter', 'none');
hold off;

subplot(2,2,2);
bar(rset);
title(sprintf('%d cells, %d models',size(D)));

disp('plot_perf_vs_tuncation: done');

