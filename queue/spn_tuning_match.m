%function res=spn_tuning_match(cellid,rawid/batchid,verbose)
%   
function res=spn_tuning_match(cellid,batchid,verbose)
    
    if ~exist('verbose','var'),
        verbose=1;
    end
    
    dbopen;
    
    % test to see if it's a rawid
    rawid=batchid;
    baphyparms=dbReadData(rawid);
    if isempty(baphyparms),
       cfd=dbbatchcells(batchid,cellid);
       if isempty(cfd),
          res=[-1 -1];
          return;
       end
       rawid=cfd(1).rawid;
       baphyparms=dbReadData(rawid);
    end
    
    [blo,bhi,tt]=spn_tuning(cellid);
    
    if isempty(blo),
        disp('cannot calculate overlap');
        res=-1;
        return
    end
    
    lblo=log2(blo);
    lbhi=log2(bhi);
    bandcount=length(baphyparms.Ref_LowFreq);
    
    for bandidx=1:bandcount,
    
        stimlo=baphyparms.Ref_LowFreq(bandidx);
        stimhi=baphyparms.Ref_HighFreq(bandidx);
        lslo=log2(stimlo);
        lshi=log2(stimhi);
        if verbose,
            fprintf('SPN range band %d: %d-%d\n',bandidx,stimlo,stimhi);
        end
        
        % fraction overlap==portion of lof-hif encompassed by band
        
        if lshi<lblo || lslo>lbhi,
            res(bandidx)=0;
        elseif lblo>=lslo && lbhi<=lshi,
            res(bandidx)=1;
        elseif lblo<lslo && lbhi<=lshi,
            res(bandidx)=1-(lslo-lblo)./(lbhi-lblo);
        elseif lblo>=lslo && lbhi>lshi,
            res(bandidx)=1-(lbhi-lshi)./(lbhi-lblo);
        else
            res(bandidx)=1-(lshi-lslo)./(lbhi-lblo);
        end
    end
    
    
