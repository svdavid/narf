function [blo,bhi,tt]=spn_tuning(cellid)
    
    dbopen;
    verbose=1;
    blo=[];
    bhi=[];
    
    tt=dbReadTuning(cellid);
    if ~isfield(tt,'bnbbf'),
        tbnb=bnb_tuning(cellid);
        if isempty(tbnb),
            tbnb.bnbbf=[];
        end
        dbWriteTuning(cellid,tbnb,1);
        tt=dbReadTuning(cellid);
    end
    if ~isfield(tt,'torbf'),
        dbtuningcheck(cellid);
        tt=dbReadTuning(cellid);
    end
    if ~isfield(tt,'lof'),
        tftc=bnb_tuning(cellid,'FTC');
        if isempty(tftc),
            tftc.lof=[];
            tftc.hif=[];
        end
        dbWriteTuning(cellid,tftc,1);
        tt=dbReadTuning(cellid);
    end
    
    blo=[];
    if ~isempty(tt.lof) && tt.lof>0,
        if verbose,
            fprintf('%s FTC:  BF=%.0f  (%.0f-%.0f)\n',...
                    cellid, tt.bf,tt.lof,tt.hif);
        end
        
        blo=tt.lof;
        bhi=tt.hif;
    end
    if isfield(tt,'torbf') && ~isempty(tt.torbf) && tt.torbf>0,
        if tt.snr>0.12,
            % require min SNR to avoid spurious tuning
            blo=round(2.^(log2(tt.torbf)-tt.bw./2));
            bhi=round(2.^(log2(tt.torbf)+tt.bw./2));
            if verbose,
                fprintf('%s TOR:  BF=%.0f  (%.0f-%.0f)\n',...
                        cellid,tt.torbf,blo,bhi);
            end
        end
    end
    if ~isempty(tt.bnbbf) && tt.bnbbf>0,
        if verbose,
            fprintf('%s BNB:  BF=%.0f  (%.0f-%.0f)\n',...
                    cellid, tt.bnbbf,tt.bnblof,tt.bnbhif);
        end
        blo=tt.bnblof;
        bhi=tt.bnbhif;
    end
