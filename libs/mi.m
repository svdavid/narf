function [I, n2]=mi(A,B,varargin) 
%MI Determines the mutual information of two images or signals
%
%   I=mi(A,B)   Mutual information of A and B, using 256 bins for
%   histograms
%   I=mi(A,B,L) Mutual information of A and B, using L bins for histograms
%
%   Assumption: 0*log(0)=0
%
%   See also ENTROPY.

%   jfd, 15-11-2006
%        01-09-2009, added case of non-double images
%        24-08-2011, speed improvements by Andrew Hill

if nargin>=3
    L=varargin{1};
else
    L=256;
end

A=double(A); 
B=double(B); 

% LB is assumed to be RESP, so it will have fewer bins (LB is ~10)
LB = min(L, length(unique(B)));
nb = hist(B(:),LB); 
nb = nb/sum(nb);

% LA = min(L, length(unique(A)));
LA = L; % 10 was too low, 15 is ok, and 20-25 is pretty stable.
na = hist(A(:),LA); 
na = na/sum(na);

n2 = hist2(A,B,LA,LB); 
n2 = n2/sum(n2(:));  % P(A,B)

papb = na' * nb;     % P(A)P(B)
I=sum(minf(n2,papb)); 

% -----------------------

function y=minf(pab,papb)
I = find(papb(:) > 1e-12 & pab(:)>1e-12);  % Pick only values > 10^-15
y = pab(I) .* log2(pab(I) ./ papb(I));     % Defn of Mutual Information
