function adp2pcprod()
% 2 adp synapses per channel, mulitplied back together. so total
% channel count doesn't change

global MODULES XXX;

append_module(MODULES.normalize_channels.mdl(struct('force_positive', true)));

sf=XXX{end}.training_set{1};
chan_count=size(XXX{end}.dat.(sf).stim,3);
init_str=repmat([1 1],[1 chan_count]);
init_tau=repmat([2 30],[1 chan_count]);
init_offset=zeros(1,chan_count);

append_module(MODULES.depression_filter_bank.mdl(...
                    struct('strength', init_str, ...
                           'tau',      init_tau, ...
                           'offset_in',init_offset,...
                           'tau_norm',  100,...
                           'facil_on', 1, ...
                           'per_channel', 1, ...
                           'fit_fields', {{'strength','tau'}})));

append_module(MODULES.add_nth_order_terms);

fns = fieldnames(XXX{end}.dat);
[T, S, C] = size(XXX{end}.dat.(fns{1}).stim);
if C==3,
    keep_channels=[3];
elseif C==10,
    %keep_channels=[1:4 6 9];
    keep_channels=[6 9];
elseif C==21,
    %keep_channels=[1:6 9 14 18];
    keep_channels=[9 14 18];
end

append_module(MODULES.subsample_channels.mdl(...
    struct('keep_channels',keep_channels)));

