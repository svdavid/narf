function condap3z1()

global MODULES STACK XXX;

% start with simple linear filter
ap3z1();
fitSubstack(length(STACK)-1);
%keyboard

%fitSubstack([],10^-3.5);
STACK{end-1}{1}.sum_channels=0;

update_xxx(2);
append_module(MODULES.cond_neuron.mdl(struct(...
    'fit_fields', {{'Vrest','V0','gL','input_threshold','input_gain'}},...
    'rectify_inputs',1)));

fitSubstack();
%fitSubstack();

