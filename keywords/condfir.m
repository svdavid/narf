function condfir()

global MODULES STACK XXX;

% initialize with simple linear filter
fir15();
fitSubstack([],10^-3);
C=size(STACK{end}{1}.coefs,1);
c1=STACK{end}{1}.coefs(1,:);
if sum(c1)<0, c1=-c1; end
STACK{end}{1}.coefs=repmat(c1,[C 1]);
STACK{end}{1}.sum_channels=0;
update_xxx(2);
append_module(MODULES.normalize_channels);

append_module(MODULES.cond_neuron.mdl(struct(...
    'fit_fields', {{'Vrest','V0','gL','input_threshold'}},...
    'rectify_inputs',1)));
%STACK{end}{1}.V0=[ones(1,Nhalf).*40 ones(1,Nchan-Nhalf).*-20];

%keyboard

fitSubstack();
%fitSubstack();


