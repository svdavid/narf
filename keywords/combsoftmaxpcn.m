function combsoftmaxpcn()

% soft-max operation

global MODULES STACK XXX FLATXXX;

inputs = {'stim'};

% Update the previous modules to make them perform channel-wise operations

C = nan; % the channel number will be defined by the first 'weight_channels' module encountered

for l = 2:length(STACK)
    if strcmp(STACK{l}{1}.name, 'weight_channels') && isnan(C)
        % get the channel number from XXX
        fns = fieldnames(XXX{l+1}.dat);
        [~,~,C] = size(XXX{l+1}.dat.(fns{1}).(STACK{l}{1}.output));
    elseif strcmp(STACK{l}{1}.name, 'nonlinearity') && ~isnan(C)
        % duplicate the "phi" parameter
        STACK{l}{1}.phi = repmat(STACK{l}{1}.phi, C, 1);
    elseif strcmp(STACK{l}{1}.name, 'depression_filter_bank') && ~isnan(C)
        % duplicate the "tau", "offset_in" and "strength" parameters
        STACK{l}{1}.tau = repmat(STACK{l}{1}.tau, [1 C]);
        STACK{l}{1}.offset_in = repmat(STACK{l}{1}.offset_in, [1 C]);
        STACK{l}{1}.strength = repmat(STACK{l}{1}.strength, [1 C]);
        % and make sure that "per_channel" is 1
        STACK{l}{1}.per_channel = 1;
    elseif strcmp(STACK{l}{1}.name, 'pole_zeros') && ~isnan(C)
        % update the "n_inputs" parameter
        STACK{l}{1}.n_inputs = C;
        % duplicate the "gains", "delays" and "poles"/"zeros" parameters
        STACK{l}{1}.gains = repmat(STACK{l}{1}.gains, [1 C]);
        STACK{l}{1}.delays = repmat(STACK{l}{1}.delays, [1 C]);
        STACK{l}{1}.poles = repmat(STACK{l}{1}.poles, [1 C]);
        STACK{l}{1}.zeros = repmat(STACK{l}{1}.zeros, [1 C]);
    elseif strcmp(STACK{l}{1}.name, 'fir_filter')
        % replace the output by "stim_filtered"
        STACK{l}{1}.sum_channels = 0;
    end
end

if strcmp(STACK{end}{1}.name, 'mean_squared_error')
    pop_module();  % trim the mean_squared_error module from the stack
end

XXX = XXX(1:2);
FLATXXX = FLATXXX(1:2);
update_xxx(1);


% this outputs to stim
append_module(MODULES.combine_fields.mdl(struct('inputs', {inputs}, ...
    'fit_fields', {{'combination_p'}}, ...
    'combination_fn', 'comb_max', ...
    'combination_p', 0.05)));
% 0.05 for \combination_p\ corresponds to a "soft" softmax. For a harder
% softmax, use 0.2 or 0.5

update_xxx(1)

% fitSubstack();
