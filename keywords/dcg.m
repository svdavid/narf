function dcg()
% Weight all input channels, producing 1 output channel
% Works on 'stim' by default. 
global MODULES STACK XXX;

signal = 'stim';
n_output_chans = 2;

% Compute number of input channels
fns = fieldnames(XXX{end}.dat);
n_input_chans = size(XXX{end}.dat.(fns{1}).(signal), 3);
phi0=repmat([0 1],[n_input_chans 1]);

% by default, don't make any of the fields fittable.  let the
% behavior-dependent fitter take care of that.
append_module(MODULES.nonlinearity.mdl(struct('phi', phi0, ...
                                              'nlfn', @nl_dcg ...
              )));
%                                              'fit_fields', {{'phi'}} ...

