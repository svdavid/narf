function mseL1()

global MODULES META STACK;
[~,wc]=find_modules(STACK,'weight_channels');
[~,fir_mod]=find_modules(STACK,'fir_filter');
append_module(MODULES.mean_squared_error.mdl(struct('output', 'score','L1Frac',0.001,...
                                                    'L1Modules',[cat(2,wc{:}) cat(2,fir_mod{:})])));
append_module(MODULES.correlation);

META.perf_metric = @pm_nmse;
