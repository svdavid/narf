% function fit05afirperfile()
%
% use SEMSE rather than standard MSE
function fit05afirperfile()

global STACK XXX

disp('NOW FITTING FIR STAGES PER FILE');

% Remove any correlation at end of stack
if strcmp(STACK{end}{1}.name, 'correlation')
    STACK = STACK(1:end-1);
    XXX = XXX(1:end-1);
end

% find STACK entries that contain linear filters
[~, fir_idxs] = find_modules(STACK, 'fir_filter', false);
if isempty(fir_idxs),
    [~, fir_idxs] = find_modules(STACK, 'pole_zeros', false);
end
if ~isempty(fir_idxs),
    % now fit linear filter stages per-filecode
    split_stack(fir_idxs{1},fir_idxs{end});
end

fit05a;


