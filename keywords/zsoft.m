function zsoft()

global MODULES;

append_module(MODULES.nonlinearity.mdl(struct('fit_fields', {{'phi'}}, ...
                                              'phi', [1 1 -2 -2], ...
                                              'nlfn', @nl_softzero)));
fitSubstack();