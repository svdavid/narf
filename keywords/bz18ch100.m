function bz18ch100()

global MODULES;

append_module(MODULES.load_stim_resps_from_baphy.mdl(...
                           struct('raw_resp_fs', 100, ...
                                  'raw_stim_fs', 100,...
                                  'stimulus_channel_count', 18, ...
                                  'data_format','bizley',...
                       'data_root','/auto/data/daq/Bizley/Level 17/Dec14/',...
                                  'include_prestim', true, ...
                                  'stimulus_format', 'ozgf'))); 

if 0,
    modules={'bz18ch100','lognn','wcg02','fir15','siglog100','fit05h','fit05a'};
    
    cellid='bz141208-05';
    estfiles={'8_12_2014 level17_N 11_18_SU2_ev_SU2_ev/Chan_05.mat_est',
              '8_12_2014 level17_N 16_14_SU2_ev_SU2_ev/Chan_05.mat_est'};
    valfiles={'8_12_2014 level17_N 11_18_SU2_ev_SU2_ev/Chan_05.mat_val',
              '8_12_2014 level17_N 16_14_SU2_ev_SU2_ev/Chan_05.mat_val'};
    fit_single_model(277, cellid, modules, estfiles,valfiles, {});
    
    cellid='bz141208-08';
    estfiles={'8_12_2014 level17_N 11_18_SU2_ev_SU2_ev/Chan_08.mat_est',};
    valfiles={'8_12_2014 level17_N 11_18_SU2_ev_SU2_ev/Chan_08.mat_val',};
    fit_single_model(277, cellid, modules, estfiles,valfiles, {});
    
    % "good" cell ... best predictions on 8/12/14
    cellid='bz141208-10';
    estfiles={'8_12_2014 level17_N 11_18_SU2_ev_SU2_ev/Chan_10.mat_est',};
    valfiles={'8_12_2014 level17_N 11_18_SU2_ev_SU2_ev/Chan_10.mat_val',};
    fit_single_model(277, cellid, modules, estfiles,valfiles, {});
    
    cellid='bz141208-10';
    estfiles={'8_12_2014 level17_N 16_14_SU2_ev_SU2_ev/Chan_10.mat_est',};
    valfiles={'8_12_2014 level17_N 16_14_SU2_ev_SU2_ev/Chan_10.mat_val',};
    fit_single_model(277, cellid, modules, estfiles,valfiles, {});
    
    %stable across days?
    cellid='bz141208-10';
    estfiles={'9_12_2014 level17_N 12_4_SU2_ev_SU2_ev/Chan_10.mat_est',};
    valfiles={'9_12_2014 level17_N 12_4_SU2_ev_SU2_ev/Chan_10.mat_val',};
    fit_single_model(277, cellid, modules, estfiles,valfiles, {});
    
    cellid='bz141208-13';
    estfiles={'raw/8_12_2014 level17_N 11_18_SU2_ev_SU2_ev/Chan_13.mat_est',};
    valfiles={'raw/8_12_2014 level17_N 11_18_SU2_ev_SU2_ev/Chan_13.mat_val',};
    fit_single_model(277, cellid, modules, estfiles,valfiles, {});
    

    fit_single_model(277, 'bz141209-04', ...
                     {'bz18ch100','lognn','wcg02','fir15','siglog100','fit05a',}, ...
                     {'9_12_2014 level17_N 16_10_SU3_ev_SU3_ev/Chan_05.mat_est',}, ...
                     {'9_12_2014 level17_N 16_10_SU3_ev_SU3_ev/Chan_05.mat_val',}, {});
    fit_single_model(277, 'bz141209-05', ...
                     {'bz18ch100','lognn','wcg02','fir15','siglog100','fit05a',}, ...
                     {'9_12_2014 level17_N 16_10_SU3_ev_SU3_ev/Chan_04.mat_est',}, ...
                     {'9_12_2014 level17_N 16_10_SU3_ev_SU3_ev/Chan_04.mat_val',}, {});

end
