% function fit05anlperfile()
%
% use SEMSE rather than standard MSE
function fit05anlperfile()

global STACK XXX

disp('NOW FITTING POST-FIR STAGES PER FILE');

% Remove any correlation at end of stack
if strcmp(STACK{end}{1}.name, 'correlation')
    STACK = STACK(1:end-1);
    XXX = XXX(1:end-1);
end

% find first STACK entry with fit_fields
ii=1;
[~, mod_idxs] = find_modules(STACK, 'nonlinearity', false);
if ~isempty(mod_idxs),
    ii=mod_idxs{end};
    
    [~,mseidx]=find_modules(STACK,'mean_squared_error');
    
    % Then boost on each file individually
    split_stack(ii,mseidx{1}-1);
end
% find STACK entries that contain linear filters
[~, dcg_idxs] = find_modules(STACK, 'nonlinearity', false);

keepidx=zeros(size(dcg_idxs));
for ii=1:length(dcg_idxs),
    if strcmp(func2str(STACK{dcg_idxs{ii}}{1}.nlfn),'nl_dcg'),
        keepidx(ii)=1;
    end;
end
dcg_idxs=dcg_idxs(find(keepidx));

behidx=1;

if length(dcg_idxs)>=behidx,
    % now fit linear filter stages per-filecode
    STACK{dcg_idxs{behidx}}{1}.fit_fields={'phi'};
    split_stack(dcg_idxs{behidx},dcg_idxs{behidx});
end


fit05a;


