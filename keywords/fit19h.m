function fit19h()
% fit19g but with with pre-spikeNL fit first

global STACK;

semse();

function fn = make_subfitter(del)
    function [a,b,c,d] = subfitter(prev_opts)    
    
        % Detect whether the fittables are in a FIR block or not    
        module_being_fit = '';
        for kk = 1:length(STACK)
            if isfield(STACK{kk}{1}, 'fit_fields') && ...
                    ~isempty(STACK{kk}{1}.fit_fields)
                module_being_fit = STACK{kk}{1}.name;
                break;
            end
        end
    
        if (strcmp(module_being_fit, 'fir_filter') || ...
            strcmp(module_being_fit, 'weight_channels')) && ...
                isempty(STACK{kk}{1}.phifn),
            if exist('prev_opts', 'var')
                [a,b,c,d] = fit_boo(prev_opts);
            else
                [a,b,c,d] = fit_boo('StopAtAbsScoreDelta', del, ...
                                    'StopAtStepNumber', 1, ...
                                    'StepAnyway', true, ...
                                    'StepGrowth', 1.3);
            end
        else
           [a,b,c,d] = fit_fminsearch(optimset('MaxIter', 1000, ...
                                               'MaxFunEvals', 1000, ...
                                               'TolFun', del, ...
                                               'TolX', del));
        end
    end
    phi = pack_fittables(STACK)';
    fprintf('del=%.7e\n',del);
    fprintf('phi=[');
    fprintf('%0.3f ', phi(:));
    fprintf(']\n');
    
    fn = @subfitter;
    
end

% Initialization: If FIR filters are all zero, initialize them randomly
[~, mod_idxs] = find_modules(STACK, 'fir_filter');
for ii = 1:length(mod_idxs)
    for jj = 1:length(STACK{mod_idxs{ii}})
        if isfield(STACK{mod_idxs{ii}}{jj}, 'fit_fields') && ...
           any(strcmp('coefs', STACK{mod_idxs{ii}}{jj}.fit_fields)) && ...
           all(0 == STACK{mod_idxs{ii}}{jj}.coefs(:))
            STACK{mod_idxs{ii}}{jj}.coefs = normrnd(0, 10^-3, size(STACK{mod_idxs{ii}}{jj}.coefs));
            fprintf('=====> Initializing FIR coefs to random numbers!\n');
        end
    end
end

%
% first fit to 10^-4 with no spike NL
%
disp('Removing static spike NL for quick fit');
[nl_mods,nl_idx] = find_modules(STACK, 'nonlinearity', false);
if strcmp(func2str(STACK{nl_idx{end}}{1}.nlfn),'nl_dcg'),
    nl_mods=nl_mods(1:(end-1));
    nl_idx=nl_idx(1:(end-1));
end
nl_save=nl_mods{end};
nl_idx=nl_idx{end};

for jj=1:length(STACK{nl_idx}),
    STACK{nl_idx}{jj}.nlfn=@nl_dummy;
    STACK{nl_idx}{jj}=rmfield(STACK{nl_idx}{jj},'fit_fields');
end

% gradually shrink the stopping criterion
scale=10^-3;
stop_at=10^-4;

while(scale > stop_at)
    fit_iteratively(make_subfitter(scale), create_term_fn('StopAtAbsScoreDelta', scale));
    scale = scale * (2/3); % Very conservative: 0.8. Probably 0.5 or even 0.25 is fine.
end

%
% restore spike NL and fit down to 10^-5.5
%
if strcmpi(func2str(nl_save{1}.nlfn),'nl_dexp'),    
    pop_module();  % remove MSE module
    pop_module();  % remove dexp
    dexp();
    % restore MSE module
    semse();
else
    disp('Restoring static spike NL for final fit');
    STACK{nl_idx}=nl_save; % restore original NL function
end
update_xxx(2);



% now fit to a lower stop criterion
scale=10^-3.5;
stop_at=10^-5.5;

while(scale > stop_at)
    fit_iteratively(make_subfitter(scale), create_term_fn('StopAtAbsScoreDelta', scale));
    scale = scale * (2/3); % Very conservative: 0.8. Probably 0.5 or even 0.25 is fine.
end

end
