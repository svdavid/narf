function m = split_stim(args)
% Jean: this is a simple module to be used in model combination
% All what it does is splitting the input stimulus ('stim' by default) into
% different outputs ('stim1' and 'stim2' by default)


% Module fields that must ALWAYS be defined
m = [];
m.mdl = @split_stim;
m.name = 'split_stim';
m.fn = @do_split_stim;
m.pretty_name = 'Split Stimulus';
m.editable_fields = {'input', 'time', 'outputs'};
m.isready_pred = @isready_always;

% Module fields that are specific to THIS MODULE
m.input =  'stim';
m.time =   'stim_time';
m.outputs = {'stim1', 'stim2'};

% Optional fields
m.plot_fns = {};
m.plot_fns{1}.fn = @do_plot_channels_as_heatmap;
m.plot_fns{1}.pretty_name = 'Normalized Channels (Heatmap)';

% Overwrite the default module fields with arguments 
if nargin > 0
    m = merge_structs(m, args);
end

% Optimize this module for tree traversal  
m.required = {m.input, m.time};   % Signal dependencies
m.modifies = {m.outputs{:}};      % These signals are modified
% ------------------------------------------------------------------------
% INSTANCE METHODS

function x = do_split_stim(mdl, x)            
    fns = fieldnames(x.dat);
    for ii = 1:length(fns)
         sf=fns{ii};
         for kk=1:length(mdl.outputs),
             x.dat.(sf).(mdl.outputs{kk}) = x.dat.(sf).(mdl.input);
         end
    end
end

% 
end
