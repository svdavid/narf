function m = lindeberg_filter_scaled(args)
% March 2014 - lienard
%
% modification of the fir_filter file

% Module fields that must ALWAYS be defined
m = [];
m.mdl = @lindeberg_filter_scaled;
m.name = 'lindeberg_filter_scaled';
m.fn = @do_lindeberg_filtering;
m.pretty_name = 'Tony Lindeberg Filter (norm. params)';
m.editable_fields = {'lincoefs', 'coefs', 'num_coefs', 'num_dims', 'baseline', ...
    'sum_channels', 'order_x', 'order_t', 'type', 'addSTRF', 'multSTRF', 'precision',...
    'input', 'filtered_input', 'time', 'output'};
m.isready_pred = @isready_always;

% Module fields that are specific to THIS MODULE
m.precision = 10; % precision in interpolation
m.order_x = 0;
m.order_t = 0;
m.type = 0; % switch between non-causal (type==0) and causal (type==1) kernels
m.num_coefs = 20; % timestep number
m.num_dims = 2;% channel number
m.baseline = 0;
m.addSTRF = 0;   % these provide two additional degrees of freedom:
m.multSTRF = 1.0001;  % finalSTRF = addSTRF + multSTRF * kernel
                 % set to Inf and Inf to normalize instead the STRF
m.baseline = 0;
m.sum_channels=2;
m.lincoefs = [0.5 0.1 log(0.01) 0.5 0];
m.lincoefs_repam = nan(size(m.lincoefs));
m.coefs = zeros(m.num_dims, m.num_coefs);
m.input =  'stim';
m.filtered_input = 'stim_filtered';
m.time =   'stim_time';
m.output = 'stim';
m.init_fit_sig = 'respavg';

% Optional fields
m.plot_gui_create_fn = @create_chan_selector_gui;
m.auto_plot = @do_plot_lindeberg_coefs_as_heatmap;
m.auto_init = @auto_init_lindeberg_filter;
m.plot_fns = {};
m.plot_fns{1}.fn = @do_plot_lindeberg_coefs_as_heatmap;
m.plot_fns{1}.pretty_name = 'FIR Coefs (Heat map)';
m.plot_fns{2}.fn = @do_plot_fir_coefs_ideal;
m.plot_fns{2}.pretty_name = 'Kernel Shape (Contour)';
m.plot_fns{3}.fn = @do_plot_all_filtered_channels;
m.plot_fns{3}.pretty_name = 'Filtered Channels (All)';
m.plot_fns{4}.fn = @do_plot_single_filtered_channel;
m.plot_fns{4}.pretty_name = 'Filtered Channels (Single)';
m.plot_fns{5}.fn = @do_plot_filter_output;
m.plot_fns{5}.pretty_name = 'FIR Output';
m.plot_fns{6}.fn = @do_plot_fir_coefs_ideal2;
m.plot_fns{6}.pretty_name = 'FIR Coefs + Kernel Shape';

% Overwrite the default module fields with arguments
if nargin > 0
    m = merge_structs(m, args);
end 

% Optimize this module for tree traversal  
m.required = {m.input, m.time, m.init_fit_sig};   % Signal dependencies
m.modifies = {m.output, m.filtered_input};        % These signals are modified

% Reset the FIR filter coefficients if its size doesn't match num_coefs
if ~isequal([m.num_dims m.num_coefs], size(m.coefs))
    m.coefs = zeros(m.num_dims, m.num_coefs);
end

% ------------------------------------------------------------------------
% INSTANCE METHODS

    function h2 = timecausal_vectorized(x, t, s, tau, v, order_x, order_t, p)
        t(t<0) = 0;
        xold=x;told=t;
        x = interp1(xold, linspace(1,length(xold),p*length(xold)));
        t = interp1(told, linspace(1,length(told),p*length(told)));
        xlen = length(x);
        tlen = length(t);
        x = repmat(x,tlen,1)';
        t = repmat(t,xlen,1);
        
        if (order_x==0)
            term1 = exp(-((x-v*t).^2)/2/s)/(2*pi*s);
        elseif (order_x==1)
            term1 = (t*v-x).*exp(-((x-v*t).^2)/2/s)/(2*pi * s^2);
        elseif (order_x==2)
            term1 = ((t*v-x).^2-s).*exp(-((x-v*t).^2)/2/s)/(2*pi * s^3);
        elseif (order_x==3)
            term1 = (t*v-x).*((x-t*v).^2-3*s).*exp(-((x-v*t).^2)/2/s)/(2*pi * s^4);
        end
        
        k=4;
        if (order_t==0)
            term2 = t.^(k-1).*exp(-t/tau)/(tau^k*factorial(k));
        elseif (order_t==1)
            term2 = tau^(-k-1)*t.^(k-2).*( (k-1)*tau-t )/(factorial(k)).*exp(-t/tau);
        elseif (order_t==2)
            term2 = tau^(-k-2)*t.^(k-3).*( (k^2-3*k+2)*tau^2-2*(k-1)*t*tau+t.^2 )/(factorial(k)).*exp(-t/tau);
        end
        term2(isnan(term2)) = 0;
        
        h = real(term1).*real(term2);
        
        if p == 1
            % no need to interpolate
            h2 = (h(1:(end-1),1:(end-1)) + h(2:end,2:end)) / 2;
        else
            h2 = blockproc(h,[p,p],@(x)mean([max(max(x.data)),min(min(x.data))]));
%             h2 = blockproc(h,[p+1,p+1],@(x)mean2(x.data));
%             h2 = blockproc(h,[p+1,p+1],@(x)max(max(x.data)));
        end
    end


    function h2 = noncausal_vectorized(x, t, s, tau, v, order_x, order_t, p)
        t(t<0) = 0;
        xold=x;told=t;

        x = interp1(xold, linspace(1,length(xold),p*length(xold)));
        t = interp1(told, linspace(1,length(told),p*length(told)));
       
        xlen = length(x);
        tlen = length(t);
        x = repmat(x,tlen,1)';
        t = repmat(t,xlen,1);
        
        if (order_x==0)
            term1 = exp(-((x-v*t).^2)/2/s)/(2*pi*s);
        elseif (order_x==1)
            term1 = (t*v-x).*exp(-((x-v*t).^2)/2/s)/(2*pi * s^2);
        elseif (order_x==2)
            term1 = ((t*v-x).^2-s).*exp(-((x-v*t).^2)/2/s)/(2*pi * s^3);
        elseif (order_x==3)
            term1 = (t*v-x).*((x-t*v).^2-3*s).*exp(-((x-v*t).^2)/2/s)/(2*pi * s^4);
        end
        
        if (order_t==0)
            term2 = tau*exp(-(tau^2)./t/2) ./ (sqrt(2*pi) * t.^(3/2));
        elseif (order_t==1)
            term2 = (tau^3-3*t*tau).*exp(-(tau^2)./t/2) ./ (2*sqrt(2*pi) * t.^(7/2));
        elseif (order_t==2)
            term2 = tau*(15*t.^2-10*t*tau^2+tau^4).*exp(-(tau^2)./t/2) ./ (4*sqrt(2*pi) * t.^(11/2));
        elseif (order_t==3)
            term2 = tau*(-105*t.^3*tau+105*t.^2*tau^3-21.*t*tau^5+tau^7).*exp(-(tau^2)./t/2) ./ (8*sqrt(2*pi) * t.^(15/2));
        end
        term2(isnan(term2)) = 0;
        
        h = real(term1).*real(term2);

        if p == 1
            % no need to interpolate
            h2 = (h(1:(end-1),1:(end-1)) + h(2:end,2:end)) / 2;
        else
            h2 = blockproc(h,[p,p],@(x)mean([max(max(x.data)),min(min(x.data))]));
%             h2 = blockproc(h,[p+1,p+1],@(x)mean2(x.data));
%             h2 = blockproc(h,[p+1,p+1],@(x)max(max(x.data)));
        end
    end


%     function h = noncausal_vectorized_old(x, t, s, tau, v, order_x, order_t)
%         xlen = length(x);
%         tlen = length(t);
%         x = repmat(x,tlen,1)';
%         t = repmat(t,xlen,1);
%         
%         if (order_x==0)
%             term1 = exp(-((x-v*t).^2)/2/s)/(2*pi*s);
% %             term1 = interp1(,@(xx) exp(-((xx-v*t).^2)/2/s)/(2*pi*s),)
%         elseif (order_x==1)
%             term1 = (t*v-x).*exp(-((x-v*t).^2)/2/s)/(2*pi * s^2);
%         elseif (order_x==2)
%             term1 = ((t*v-x).^2-s).*exp(-((x-v*t).^2)/2/s)/(2*pi * s^3);
%         elseif (order_x==3)
%             term1 = (t*v-x).*((x-t*v).^2-3*s).*exp(-((x-v*t).^2)/2/s)/(2*pi * s^4);
%         end
%         
%         if (order_t==0)
%             term2 = tau*exp(-(tau^2)./t/2) ./ (sqrt(2*pi) * t.^(3/2));
%         elseif (order_t==1)
%             term2 = (tau^3-3*t*tau).*exp(-(tau^2)./t/2) ./ (2*sqrt(2*pi) * t.^(7/2));
%         elseif (order_t==2)
%             term2 = tau*(15*t.^2-10*t*tau^2+tau^4).*exp(-(tau^2)./t/2) ./ (4*sqrt(2*pi) * t.^(11/2));
%         elseif (order_t==3)
%             term2 = tau*(-105*t.^3*tau+105*t.^2*tau^3-21.*t*tau^5+tau^7).*exp(-(tau^2)./t/2) ./ (8*sqrt(2*pi) * t.^(15/2));
%         end
%         
%         h = term1.*term2;
%         h(t<=0) = 0;
%     end


    function mdl = constrain_lincoefs(mdl)
        %xshift
        mdl.lincoefs_repam(1) = min(mdl.lincoefs(1), 1);
        mdl.lincoefs_repam(1) = max(mdl.lincoefs(1), 0);
        
        %tshift
        mdl.lincoefs_repam(2) = min(mdl.lincoefs(2), 0.75);
        mdl.lincoefs_repam(2) = max(mdl.lincoefs(2), 0);
        
        % Here are some constraints
        %s
        mdl.lincoefs_repam(3) = min(mdl.lincoefs(3), log(0.2));
        mdl.lincoefs_repam(3) = max(mdl.lincoefs(3), log(0.001));
        mdl.lincoefs_repam(3) = exp(mdl.lincoefs(3)); % !!! log-transformation happens here
        
        % tau
        mdl.lincoefs_repam(4) = min(mdl.lincoefs(4), 1);
        mdl.lincoefs_repam(4) = max(mdl.lincoefs(4), 0.1);
        
        % v
        if ((isfield(mdl,'noglissando')) && (mdl.noglissando==1))
            mdl.lincoefs_repam(5) = 0;
        else
            mdl.lincoefs_repam(5) = min(mdl.lincoefs(5), 10);
            mdl.lincoefs_repam(5) = max(mdl.lincoefs(5), -10);
        end
    end


    function coefs = initialize_coefs(num_dims, num_coefs, xshift, tshift, s, tau, v, order_x, order_t, type, addSTRF, multSTRF, p)
        if (type==0)
            coefs = noncausal_vectorized((0:num_dims)/num_dims-xshift, (0:num_coefs)/num_coefs-tshift, s, tau, v, order_x, order_t, p);
        elseif (type==1)
            coefs = timecausal_vectorized((0:num_dims)/num_dims-xshift, (0:num_coefs)/num_coefs-tshift, s, tau, v, order_x, order_t, p);
        end
        if addSTRF == Inf && multSTRF == Inf
            coefs = coefs / sum(sum(coefs));
        else
            coefs = addSTRF + multSTRF * coefs;
        end
    end

    function mdl = auto_init_lindeberg_filter(stack, xxx)
        % NOTE: Unlike most plot functions, auto_init functions get a
        % STACK and XXX which do not yet have this module or module's data
        % added to them.
        if ~isfield(m, 'fit_fields')
            return
        end
        
        % Init the coefs to have the right dimensionality
        mdl = m;
        x = xxx{end};
        fns = fieldnames(x.dat);
        sf = fns{1};
        [~, ~, C] = size(x.dat.(sf).(mdl.input));
        mdl.num_dims = C;
        
        % constrain the coefficients
        mdl = constrain_lincoefs(mdl);
        
        xshift = mdl.lincoefs_repam(1);
        tshift = mdl.lincoefs_repam(2);
        s = mdl.lincoefs_repam(3);
        tau = mdl.lincoefs_repam(4);
        v = mdl.lincoefs_repam(5);
        if ~isfield(mdl,'type'),
            type = 0;
        else
            type = mdl.type;
        end
        mdl.coefs = initialize_coefs(mdl.num_dims, mdl.num_coefs, xshift, tshift, s, tau, v, mdl.order_x, mdl.order_t, type, mdl.addSTRF, mdl.multSTRF, mdl.precision);
    end

    function x = do_lindeberg_filtering(mdl, x)
        % Apply the FIR filter across every stimfile
        if ~isfield(mdl, 'sum_channels')
            mdl.sum_channels=2;
        end
        
        fns = fieldnames(x.dat);
        for ii = 1:length(fns)
            sf=fns{ii};
            
            mdl = constrain_lincoefs(mdl);
            
            [T, S, C] = size(x.dat.(sf).(mdl.input));
            if ~isequal(C, mdl.num_dims)
                warning('Updated the dimensions of (mdl.input) to match channel count.');
                mdl.num_dims = C;
            end

            
            xshift = mdl.lincoefs_repam(1);
            tshift = mdl.lincoefs_repam(2);
            s = mdl.lincoefs_repam(3);
            tau = mdl.lincoefs_repam(4);
            v = mdl.lincoefs_repam(5);
            if ~isfield(mdl,'type'),
                type = 0;
            else
                type = mdl.type;
            end
            coefs = initialize_coefs(mdl.num_dims, mdl.num_coefs, xshift, tshift, s, tau, v, mdl.order_x, mdl.order_t, type, mdl.addSTRF, mdl.multSTRF, mdl.precision);
            
            % Have a space allocated for computing initial filter conditions
%             init_data_space = ones(size(coefs, 2) * 2, 1);
            
            
            
            tmp = zeros(T, S, C);
%             tmp2 = zeros(T, S, C);
            
% % will a normalization destroy everything?
%             for s = 1:S
%                 n = zeros(size(x.dat.(sf).(mdl.input)(:, s, 1)));
%                 for c = 1:C
%                     n = n + x.dat.(sf).(mdl.input)(:, s, c);
%                 end
%                 mask = ((~isnan(n)) & (n>0));
%                 for c = 1:C
%                     x.dat.(sf).(mdl.input)(mask, s, c) = x.dat.(sf).(mdl.input)(mask, s, c) ./ n(mask);
%                     x.dat.(sf).(mdl.input)(~mask, s, c) = 0;
%                 end
%             end

            
            
            % Filter!
            for s = 1:S
                for c = 1:C,
%                     % Find proper initial conditions for the filter
%                     [~, Zf] = filter(coefs(c,:)', 1, ...
%                         init_data_space .* x.dat.(sf).(mdl.input)(1, s, c));
%                     
%                     tmp(:, s, c) = filter(coefs(c,:)', 1, ...
%                         x.dat.(sf).(mdl.input)(:, s, c), ...
%                         Zf);
                    
                    % as a matter of fact it is 50% faster to not compute the
                    % initial conditions
                    tmp(:, s, c) = filter(coefs(c,:)', 1, ...
                        x.dat.(sf).(mdl.input)(:, s, c));
                end
            end
            
            % Now the input has been filtered.
            x.dat.(sf).(mdl.filtered_input) = tmp;
            
            if mdl.sum_channels == 1
                % The output is the sum of the filtered channels
                x.dat.(sf).(mdl.output) = squeeze(sum(tmp, 3)) + mdl.baseline;
            elseif mdl.sum_channels == 2
                % the overall mean is more useful as it allows changing the size of filters
                x.dat.(sf).(mdl.output) = squeeze(sum(tmp, 3))/ (mdl.num_coefs) / (mdl.num_dims) + mdl.baseline;
            else
                x.dat.(sf).(mdl.output) = tmp + mdl.baseline;
            end
            
%             fprintf('\nChannel inputs in set %s:\n',sf);
%             tmp_var = squeeze(mean(mean(x.dat.(sf).(mdl.input))))'
%             
%             fprintf('\nOutput in set %s:\n',sf);
%             tmp_var = mean(x.dat.(sf).(mdl.output))
%             
%             fprintf('\nMean coefs of %s:\n', sf);
%             tmp_var = mean(mean(coefs))
% 
%             fprintf('\nCoefs gen arguments:\n');
%             tmp_var = [mdl.num_dims, mdl.num_coefs, xshift, tshift, s, tau, v, mdl.order_x, mdl.order_t, type, mdl.addSTRF, mdl.multSTRF, mdl.precision]
        end
    end

% ------------------------------------------------------------------------
% Plot methods

    function do_plot_lindeberg_coefs_as_heatmap(sel, stack, xxx)
        mdls = stack{end};
        
        % Find the min and max values so colors are scaled appropriately
        c_max = 0;
        for ii = 1:length(mdls)
            
            mdls{ii} = constrain_lincoefs(mdls{ii});
            
            xshift = mdls{ii}.lincoefs_repam(1); %xshift = min(xshift,10);
            tshift = mdls{ii}.lincoefs_repam(2); %tshift = min(tshift,10);
            s = mdls{ii}.lincoefs_repam(3);
            tau = mdls{ii}.lincoefs_repam(4);
            v = mdls{ii}.lincoefs_repam(5);
            if ~isfield(mdls{ii},'type'),
                type = 0;
            else
                type = mdls{ii}.type;
            end
            coefs = initialize_coefs(mdls{ii}.num_dims, mdls{ii}.num_coefs, xshift, tshift, s, tau, v, mdls{ii}.order_x, mdls{ii}.order_t, type, mdls{ii}.addSTRF, mdls{ii}.multSTRF, mdls{ii}.precision);
            c_max = max(c_max, max(abs(coefs(:))));
        end
        
        % Plot all parameter sets' coefficients. Separate them by white pixels.
        xpos = 1;
        hold on;
        for ii = 1:length(mdls)
            
            % already constrained
%             mdls{ii} = constrain_lincoefs(mdls{ii});
            
            xshift = mdls{ii}.lincoefs_repam(1); %xshift = min(xshift,10);
            tshift = mdls{ii}.lincoefs_repam(2); %tshift = min(tshift,10);
            s = mdls{ii}.lincoefs_repam(3);
            tau = mdls{ii}.lincoefs_repam(4);
            v = mdls{ii}.lincoefs_repam(5);
            if ~isfield(mdls{ii},'type'),
                type = 0;
            else
                type = mdls{ii}.type;
            end
            coefs = initialize_coefs(mdls{ii}.num_dims, mdls{ii}.num_coefs, xshift, tshift, s, tau, v, mdls{ii}.order_x, mdls{ii}.order_t, type, mdls{ii}.addSTRF, mdls{ii}.multSTRF, mdls{ii}.precision);
            
            [w, h] = size(coefs');
            imagesc([xpos xpos+w-1], [1, h], coefs, [-c_max-eps c_max+eps]);
            
%             coefs2 = initialize_coefs(mdls{ii}.num_dims*10, mdls{ii}.num_coefs*10, xshift, tshift, s, tau, v, mdls{ii}.order_x, mdls{ii}.order_t, type, mdls{ii}.addSTRF, mdls{ii}.multSTRF, 1);
%             contourf(15.5+linspace(0.5,mdls{ii}.num_coefs+0.5,mdls{ii}.num_coefs*10), linspace(0.5,mdls{ii}.num_dims+1.5,mdls{ii}.num_dims*10), coefs2);
%             plot(15.5+15,1,'.');
            
%             text(xpos, 1, sprintf('Sparsity: %f\nSmoothness: %f', ...
%                 sparsity_metric(coefs), ...
%                 smoothness_metric(coefs)));
            xpos = xpos + 1 + size(mdls{ii}.coefs, 2);
        end
        hold off;
        set(gca,'YDir','normal');
        ca = caxis;
        lim = max(abs(ca));
        caxis([-lim, +lim]);
        axis tight;
%         do_xlabel('Filter Time Index (ms)');
%         do_ylabel('Filter Frequency Index (Hz)');

        
        
        xlabel('Filter Time Index (ms)');
        ylabel('Filter Frequency Index (Hz)');
        
        xticklabels = 0:10:150;
        xticks = linspace(1, size(coefs, 2), numel(xticklabels));
        set(gca, 'XTick', xticks, 'XTickLabel', xticklabels);
        
        yticklabels = [200; 20000];
        yticks = linspace(1, size(coefs, 1), numel(yticklabels));
        set(gca, 'YTick', yticks, 'YTickLabel', yticklabels)
    end

    function do_plot_all_filtered_channels(sel, stack, xxx)
        [mdls, xins, xouts] = calc_paramsets(stack, xxx(1:end));
        sel.chan_idx = []; % when chan_idx is empty, do_plot plots all channels
        do_plot(xouts, mdls{1}.time, mdls{1}.filtered_input, ...
            sel, 'Time [s]', 'Filtered Channel [-]');
    end

    function do_plot_single_filtered_channel(sel, stack, xxx)
        [mdls, xins, xouts] = calc_paramsets(stack, xxx(1:end));
        do_plot(xouts, mdls{1}.time, mdls{1}.output, ...
            sel, 'Time [s]', 'Filtered Channel [-]');
    end

    function do_plot_filter_output(sel, stack, xxx)
        [mdls, xins, xouts] = calc_paramsets(stack, xxx(1:end));
        sel.chan_idx = []; % when chan_idx is empty, do_plot plots all channels
        do_plot(xouts, mdls{1}.time, mdls{1}.output, ...
            sel, 'Time [s]', 'FIR Output [-]');
        axis auto;
        set(gca, 'XTickMode', 'auto', 'XTickLabelMode', 'auto');
        set(gca, 'YTickMode', 'auto', 'YTickLabelMode', 'auto');
    end


    function do_plot_fir_coefs_ideal(sel, stack, xxx)
        mdls = stack{end};
        
        % Find the min and max values so colors are scaled appropriately
        c_max = 0;
        for ii = 1:length(mdls)
            
            mdls{ii} = constrain_lincoefs(mdls{ii});
            
            xshift = mdls{ii}.lincoefs_repam(1); %xshift = min(xshift,10);
            tshift = mdls{ii}.lincoefs_repam(2); %tshift = min(tshift,10);
            s = mdls{ii}.lincoefs_repam(3);
            tau = mdls{ii}.lincoefs_repam(4);
            v = mdls{ii}.lincoefs_repam(5);
            if ~isfield(mdls{ii},'type'),
                type = 0;
            else
                type = mdls{ii}.type;
            end
%             coefs = initialize_coefs(mdls{ii}.num_dims, mdls{ii}.num_coefs, xshift, tshift, s, tau, v, mdls{ii}.order_x, mdls{ii}.order_t, type, mdls{ii}.addSTRF, mdls{ii}.multSTRF, mdls{ii}.precision);
%             c_max = max(c_max, max(abs(coefs(:))));
            coefs2 = initialize_coefs(mdls{ii}.num_dims*10, mdls{ii}.num_coefs*10, xshift, tshift, s, tau, v, mdls{ii}.order_x, mdls{ii}.order_t, type, mdls{ii}.addSTRF, mdls{ii}.multSTRF, 1);
            c_max = max(c_max, max(abs(coefs2(:))));
        end
        
        % Plot all parameter sets' coefficients. Separate them by white pixels.
        xpos = 1;
        hold on;
        for ii = 1:length(mdls)
            
            % already constrained
%             mdls{ii} = constrain_lincoefs(mdls{ii});
            
            xshift = mdls{ii}.lincoefs_repam(1); %xshift = min(xshift,10);
            tshift = mdls{ii}.lincoefs_repam(2); %tshift = min(tshift,10);
            s = mdls{ii}.lincoefs_repam(3);
            tau = mdls{ii}.lincoefs_repam(4);
            v = mdls{ii}.lincoefs_repam(5);
            if ~isfield(mdls{ii},'type'),
                type = 0;
            else
                type = mdls{ii}.type;
            end
            coefs = initialize_coefs(mdls{ii}.num_dims, mdls{ii}.num_coefs, xshift, tshift, s, tau, v, mdls{ii}.order_x, mdls{ii}.order_t, type, mdls{ii}.addSTRF, mdls{ii}.multSTRF, mdls{ii}.precision);
            
%             [w, h] = size(coefs');
%             imagesc([xpos xpos+w-1], [1, h], coefs, [-c_max-eps c_max+eps]);
            
            coefs2 = initialize_coefs(mdls{ii}.num_dims*10, mdls{ii}.num_coefs*10, xshift, tshift, s, tau, v, mdls{ii}.order_x, mdls{ii}.order_t, type, mdls{ii}.addSTRF, mdls{ii}.multSTRF, 1);
            contourf(linspace(0.5,mdls{ii}.num_coefs+0.5,mdls{ii}.num_coefs*10), linspace(0.5,mdls{ii}.num_dims+1.5,mdls{ii}.num_dims*10), coefs2,100);
            contour(linspace(0.5,mdls{ii}.num_coefs+0.5,mdls{ii}.num_coefs*10), linspace(0.5,mdls{ii}.num_dims+1.5,mdls{ii}.num_dims*10), coefs2,100);

%             text(xpos, 1, sprintf('Sparsity: %f\nSmoothness: %f', ...
%                 sparsity_metric(coefs), ...
%                 smoothness_metric(coefs)));
            xpos = xpos + 1 + size(mdls{ii}.coefs, 2);
        end
        hold off;
        set(gca,'YDir','normal');
        ca = caxis;
        lim = max(abs(ca));
        caxis([-lim, +lim]);
        axis tight;
%         do_xlabel('Filter Time Index (ms)');
%         do_ylabel('Filter Frequency Index (Hz)');

        
        
        xlabel('Filter Time Index (ms)');
        ylabel('Filter Frequency Index (Hz)');
        
        xticklabels = 0:10:150;
        xticks = linspace(1, size(coefs, 2), numel(xticklabels));
        set(gca, 'XTick', xticks, 'XTickLabel', xticklabels);
        
        yticklabels = [200; 20000];
        yticks = linspace(1, size(coefs, 1), numel(yticklabels));
        set(gca, 'YTick', yticks, 'YTickLabel', yticklabels)
    end

    function do_plot_fir_coefs_ideal2(sel, stack, xxx)
        mdls = stack{end};
        
        % Find the min and max values so colors are scaled appropriately
        c_max = 0;
        for ii = 1:length(mdls)
            
            mdls{ii} = constrain_lincoefs(mdls{ii});
            
            xshift = mdls{ii}.lincoefs_repam(1); %xshift = min(xshift,10);
            tshift = mdls{ii}.lincoefs_repam(2); %tshift = min(tshift,10);
            s = mdls{ii}.lincoefs_repam(3);
            tau = mdls{ii}.lincoefs_repam(4);
            v = mdls{ii}.lincoefs_repam(5);
            if ~isfield(mdls{ii},'type'),
                type = 0;
            else
                type = mdls{ii}.type;
            end
            coefs = initialize_coefs(mdls{ii}.num_dims, mdls{ii}.num_coefs, xshift, tshift, s, tau, v, mdls{ii}.order_x, mdls{ii}.order_t, type, mdls{ii}.addSTRF, mdls{ii}.multSTRF, mdls{ii}.precision);
            c_max = max(c_max, max(abs(coefs(:))));
            coefs2 = initialize_coefs(mdls{ii}.num_dims*10, mdls{ii}.num_coefs*10, xshift, tshift, s, tau, v, mdls{ii}.order_x, mdls{ii}.order_t, type, mdls{ii}.addSTRF, mdls{ii}.multSTRF, 1);
            c_max = max(c_max, max(abs(coefs2(:))));

        end
        
        % Plot all parameter sets' coefficients. Separate them by white pixels.
        xpos = 1;
        hold on;
        for ii = 1:length(mdls)
            
            % already constrained
%             mdls{ii} = constrain_lincoefs(mdls{ii});
            
            xshift = mdls{ii}.lincoefs_repam(1); %xshift = min(xshift,10);
            tshift = mdls{ii}.lincoefs_repam(2); %tshift = min(tshift,10);
            s = mdls{ii}.lincoefs_repam(3);
            tau = mdls{ii}.lincoefs_repam(4);
            v = mdls{ii}.lincoefs_repam(5);
            if ~isfield(mdls{ii},'type'),
                type = 0;
            else
                type = mdls{ii}.type;
            end
            coefs = initialize_coefs(mdls{ii}.num_dims, mdls{ii}.num_coefs, xshift, tshift, s, tau, v, mdls{ii}.order_x, mdls{ii}.order_t, type, mdls{ii}.addSTRF, mdls{ii}.multSTRF, mdls{ii}.precision);
            
            [w, h] = size(coefs');
            imagesc([xpos xpos+w-1], [1, h], coefs, [-c_max-eps c_max+eps]);
            
            coefs2 = initialize_coefs(mdls{ii}.num_dims*10, mdls{ii}.num_coefs*10, xshift, tshift, s, tau, v, mdls{ii}.order_x, mdls{ii}.order_t, type, mdls{ii}.addSTRF, mdls{ii}.multSTRF, 1);
            contour(linspace(0.5,mdls{ii}.num_coefs+0.5,mdls{ii}.num_coefs*10), linspace(0.5,mdls{ii}.num_dims+1.5,mdls{ii}.num_dims*10), coefs2);
            
%             text(xpos, 1, sprintf('Sparsity: %f\nSmoothness: %f', ...
%                 sparsity_metric(coefs), ...
%                 smoothness_metric(coefs)));
            xpos = xpos + 1 + size(mdls{ii}.coefs, 2);
        end
        hold off;
        set(gca,'YDir','normal');
        ca = caxis;
        lim = max(abs(ca));
        caxis([-lim, +lim]);
        axis tight;
%         do_xlabel('Filter Time Index (ms)');
%         do_ylabel('Filter Frequency Index (Hz)');

        
        
        xlabel('Filter Time Index (ms)');
        ylabel('Filter Frequency Index (Hz)');
        
        xticklabels = 0:10:150;
        xticks = linspace(1, size(coefs, 2), numel(xticklabels));
        set(gca, 'XTick', xticks, 'XTickLabel', xticklabels);
        
        yticklabels = [200; 20000];
        yticks = linspace(1, size(coefs, 1), numel(yticklabels));
        set(gca, 'YTick', yticks, 'YTickLabel', yticklabels)
    end


end
